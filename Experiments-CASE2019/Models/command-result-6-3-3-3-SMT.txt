java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --upal2pta bioman-linear-compact-6.xml 


./bioman --ccs-smt 60 ProdA1 ENDJOB ProdA2 ENDJOB ProdA3 ENDJOB -Bio11 -Idle -Bio12 -Idle -Bio13 -Idle -Pur1 -Idle -Pur2 -Idle -Pur3 -Idle -Clean1 -Idle -Clean2 -Idle -Clean3 -Idle ProdA4 ENDJOB ProdA5 ENDJOB ProdA6 ENDJOB 


java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta  --iterative--internalOptimizer 30 60 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Pur2 -Pur3 -Clean1 -Clean2 -Clean3 ProdA4 ProdA5 ProdA6 


java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta  --iterative--internalOptimizer 30 60 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Pur2 -Pur3 -Clean1 -Clean2 -Clean3 ProdA4 ProdA5 ProdA6
Reading from SMT file: code.smt2
using process: ProdA1
using process: ProdA2
using process: ProdA3
using process: ProdA4
using process: ProdA5
using process: ProdA6
Reading solver's configurations from solver.txt ...
>>> Model has Real optimization declarations.
SMT iterative code is segmented.
Finding solution in the range of [30,60] steps...
[30,60] running solver...
Iteration: 45 -> SATISFIABLE
ProdA1_price_45:   		40
ProdA2_price_45:   		160
ProdA3_price_45:   		80
ProdA4_price_45:   		160
ProdA5_price_45:   		40
ProdA6_price_45:   		68
[30,44] running solver...
Iteration: 37 -> SATISFIABLE
ProdA1_price_37:   		190
ProdA2_price_37:   		240
ProdA3_price_37:   		40
ProdA4_price_37:   		80
ProdA5_price_37:   		40
ProdA6_price_37:   		40
[30,36] running solver...
Iteration: 33 -> SATISFIABLE
ProdA1_price_33:   		190
ProdA2_price_33:   		40
ProdA3_price_33:   		240
ProdA4_price_33:   		80
ProdA5_price_33:   		40
ProdA6_price_33:   		80
[30,32] running solver...
Iteration: 31 -> SATISFIABLE
ProdA1_price_31:   		60
ProdA2_price_31:   		90
ProdA3_price_31:   		40
ProdA4_price_31:   		62
ProdA5_price_31:   		62
ProdA6_price_31:   		140
[30,30] running solver...
Iteration: 30 -> SATISFIABLE
ProdA1_price_30:   		80
ProdA2_price_30:   		40
ProdA3_price_30:   		240
ProdA4_price_30:   		240
ProdA5_price_30:   		60
ProdA6_price_30:   		80
Minimum number of steps: 30
sample used SMT code saved as test.smt2
Optimization is activated.
Z3 time out set to: 6000000
SATISFIABLE
ProdA1_price_30= 40
ProdA2_price_30= 40
ProdA3_price_30= 40
ProdA4_price_30= 40
ProdA5_price_30= 40
ProdA6_price_30= 40
Total sum: 240.0
SATISFIABLE
ProdA1_price_30:   		40
ProdA2_price_30:   		40
ProdA3_price_30:   		40
ProdA4_price_30:   		40
ProdA5_price_30:   		40
ProdA6_price_30:   		40
Total sum: 240.0
number of functions: 6224
Total used function definitions: 2324


=========================================


>>>Preparing and executing time by Z3 SMT solver: 		475282 mil sec


===========================================================================




java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta  --iterative--internalOptimizer 10 30 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Pur2 -Pur3 -Clean1 -Clean2 -Clean3 ProdA4 ProdA5 ProdA6
Reading from SMT file: code.smt2
using process: ProdA1
using process: ProdA2
using process: ProdA3
using process: ProdA4
using process: ProdA5
using process: ProdA6
Reading solver's configurations from solver.txt ...
>>> Model has Real optimization declarations.
SMT iterative code is segmented.
Finding solution in the range of [10,30] steps...
[10,30] running solver...
Iteration: 20 -> SATISFIABLE
ProdA1_price_20:   		40
ProdA2_price_20:   		240
ProdA3_price_20:   		40
ProdA4_price_20:   		40
ProdA5_price_20:   		40
ProdA6_price_20:   		40
[10,19] running solver...
Iteration: 14 -> UNSATISFIABLE
[15,19] running solver...
Iteration: 17 -> UNSATISFIABLE
[18,19] running solver...
Iteration: 18 -> SATISFIABLE
ProdA1_price_18:   		95
ProdA2_price_18:   		80
ProdA3_price_18:   		80
ProdA4_price_18:   		80
ProdA5_price_18:   		95
ProdA6_price_18:   		95
Minimum number of steps: 18
sample used SMT code saved as test.smt2
Optimization is activated.
Z3 time out set to: 6000000
SATISFIABLE
ProdA1_price_18= 60
ProdA2_price_18= 60
ProdA3_price_18= 60
ProdA4_price_18= 60
ProdA5_price_18= 60
ProdA6_price_18= 60
Total sum: 360.0
SATISFIABLE
ProdA1_price_18:   		60
ProdA2_price_18:   		60
ProdA3_price_18:   		60
ProdA4_price_18:   		60
ProdA5_price_18:   		60
ProdA6_price_18:   		60
Total sum: 360.0
number of functions: 3752
Total used function definitions: 1440


=========================================


>>>Preparing and executing time by Z3 SMT solver: 		79940 mil sec


===============================================================================

java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta  --iterative--internalOptimizer 19 19 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Pur2 -Pur3 -Clean1 -Clean2 -Clean3 ProdA4 ProdA5 ProdA6
Reading from SMT file: code.smt2
using process: ProdA1
using process: ProdA2
using process: ProdA3
using process: ProdA4
using process: ProdA5
using process: ProdA6
Reading solver's configurations from solver.txt ...
>>> Model has Real optimization declarations.
SMT iterative code is segmented.
Finding solution in the range of [19,19] steps...
[19,19] running solver...
Iteration: 19 -> SATISFIABLE
ProdA1_price_19:   		40
ProdA2_price_19:   		40
ProdA3_price_19:   		62
ProdA4_price_19:   		62
ProdA5_price_19:   		62
ProdA6_price_19:   		40
Minimum number of steps: 19
sample used SMT code saved as test.smt2
Optimization is activated.
Z3 time out set to: 6000000
SATISFIABLE
ProdA1_price_19= 40
ProdA2_price_19= 40
ProdA3_price_19= 40
ProdA4_price_19= 40
ProdA5_price_19= 40
ProdA6_price_19= 40
Total sum: 240.0
SATISFIABLE
ProdA1_price_19:   		40
ProdA2_price_19:   		40
ProdA3_price_19:   		40
ProdA4_price_19:   		40
ProdA5_price_19:   		40
ProdA6_price_19:   		40
Total sum: 240.0
number of functions: 3958
Total used function definitions: 1512


=========================================


>>>Preparing and executing time by Z3 SMT solver: 		54806 mil sec
