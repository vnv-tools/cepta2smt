
java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --upal2pta bioman-linear-compact-3-3-1-1-SMT.xml

.\bioman --ccs-smt 40 ProdA1 ENDJOB ProdA2 ENDJOB ProdA3 ENDJOB -Bio11 -wait -Bio12 -wait -Bio13 -wait -Pur1 -wait -Clean1 -wait

java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta --iterative 10 40 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Clean1

java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta --iterative--internalOptimizer 22 22 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Clean1



java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta --iterative 10 40 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Clean1
Reading from SMT file: code.smt2
using process: ProdA1
using process: ProdA2
using process: ProdA3
Reading solver's configurations from solver.txt ...
>>> Model has Real optimization declarations.
SMT iterative code is segmented.
Finding solution in the range of [10,40] steps...
running solver...
Iteration: 25 -> SATISFIABLE
ProdA1_price_25:                90
ProdA2_price_25:                40
ProdA3_price_25:                240
running solver...
Iteration: 17 -> UNSATISFIABLE
running solver...
Iteration: 21 -> UNSATISFIABLE
running solver...
Iteration: 23 -> SATISFIABLE
ProdA1_price_23:                60
ProdA2_price_23:                40
ProdA3_price_23:                195
running solver...
Iteration: 22 -> SATISFIABLE
ProdA1_price_22:                60
ProdA2_price_22:                40
ProdA3_price_22:                295
running solver...
Iteration: 21 -> UNSATISFIABLE
*Iteration: 22 -> SATISFIABLE
Minimum number of steps: 22
sample used SMT code saved as test.smt2
Optimization is activated.
SATISFIABLE
ProdA1_price_22:                60
ProdA2_price_22:                40
ProdA3_price_22:                295
Total sum: 395.0
number of functions: 2375
Total used function definitions: 865


=========================================


>>>Preparing and executing time by Z3 SMT solver:               36963 mil sec


-----------------------------------------------------------------------------------------------



 java -jar uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta --iterative--internalOptimizer 25 25 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Clean1
Reading from SMT file: code.smt2
using process: ProdA1
using process: ProdA2
using process: ProdA3
Reading solver's configurations from solver.txt ...
>>> Model has Real optimization declarations.
SMT iterative code is segmented.
Finding solution in the range of [25,25] steps...
running solver...
Iteration: 25 -> SATISFIABLE
ProdA1_price_25:                90
ProdA2_price_25:                40
ProdA3_price_25:                240
Minimum number of steps: 25
sample used SMT code saved as test.smt2
Optimization is activated.
Z3 time out set to: 6000000
SATISFIABLE
ProdA1_price_25= 40
ProdA2_price_25= 40
ProdA3_price_25= 140
Total sum: 220.0
SATISFIABLE
ProdA1_price_25:                40
ProdA2_price_25:                40
ProdA3_price_25:                140
Total sum: 220.0
number of functions: 2696
Total used function definitions: 976


=========================================


>>>Preparing and executing time by Z3 SMT solver:               56905 mil sec