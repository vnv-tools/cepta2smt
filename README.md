
<p>The Bio-Manufacturing project goal is to enable efficient operations of biopharmaceutical production systems when arbitrary configuration and setups considered concerning continuous manufacturing and use of disposable equipment.&nbsp;<span style="color:#26282a">For a discussion on the theory behind the tool, please refer to our <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/CASE2019__Task_Scheduling_with_Nonlinear_Costs_using_SMT_Solvers.pdf">CASE 2019 paper</a>.&nbsp;If you decide to use our work, then please cite the paper:</span><br />
<span style="color:#26282a">&nbsp;</span><br />
<span style="color:#26282a">@<a href="/wiki/show/bio-manufacturing/InProceedings">InProceedings</a>{<a href="/wiki/show/bio-manufacturing/HekmatnejadPF2019case">HekmatnejadPF2019case</a>,</span><br />
<span style="color:#26282a">&nbsp; author&nbsp;&nbsp;&nbsp; = {Mohammad Hekmatnejad and Giulia Pedrielli and Georgios Fainekos},</span><br />
<span style="color:#26282a">&nbsp; title&nbsp;&nbsp;&nbsp;&nbsp; = {Optimal Task Scheduling with Nonlinear Costs using SMT Solvers},</span><br />
<span style="color:#26282a">&nbsp; booktitle = {IEEE International Conference on Automation Science and Engineering ({CASE})},</span><br />
<span style="color:#26282a">&nbsp; year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = {2019},</span><br />
<span style="color:#26282a">}</span><br />
<br />
<span style="color:#26282a">Our tool is based on the code by <a href="https://ashut.bitbucket.io">Trivedi&rsquo;s group</a>.</span></p>

<h2 style="font-style:italic">CEPTA2SMT</h2>
&nbsp;

<h3>Pre/Post Processing Modules</h3>
One submodule (preprocessor) translates the processes that are developed in Uppaal into an XML format to be compatible with our PTA format.<br />
Another submodule (postprocessor) translates a produced SMT model into an equivalent trace which is annotated with timesteps and sets of activated events and actions.<br />
&nbsp;
<h3>Priced Timed Automata&nbsp;to SMT Code&nbsp;</h3>
This module translates the modedel processes in the form of priced time automata into an equivalent SMT code.<br />
&nbsp;
<h3>SMT-based Task Scheduler</h3>
This module uses <a href="https://github.com/Z3Prover/z3/wiki">SMT solver Z3</a> to find an optimal/sub-optimal schedule for the SMT-based modeled processes. This tool has parameters for different type of execution:

<ul>
	<li>finding a random trace&nbsp;within a given horizon.</li>
	<li>finding the shortest&nbsp;trace&nbsp;within a given&nbsp;interval as horizons.</li>
	<li>finding an&nbsp;optimal shortest trace within a given&nbsp;interval as horizons. We use Z3 optimal solver to find an optimal trace, and it might return UNKNOWN if it cannot find an optimal solution.</li>
	<li>finding&nbsp;an&nbsp;optimal shortest trace within a given&nbsp;interval as horizons and a given interval for costs.</li>
</ul>


<h1>Requirements:</h1>

<p>- JVM 1.8 (64bit) or later</p>

<p>- Apache Maven 3.5 or later</p>

<p>- Z3 SMT solver 4.5&nbsp;(64bit)&nbsp;or later (dReal 4.18 or later is optional&nbsp;)<br />
<br />
- C++ compiler and linker (gcc/g++/clang)<br />
<br />
- CMake 3.12 or later<br />
&nbsp;</p>

<h1>Installation</h1>

<p>1- Download the project from either</p>

<p>Using git address:&nbsp; run the command: git clone git@git.assembla.com:cpslab/bio-manufacturing.git</p>

<p>or directly download the zip file from the top-left corner of the page located at:&nbsp;<a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source">https://cpslab.assembla.com/spaces/bio-manufacturing/git/source</a>&nbsp;&nbsp;and extract the files.</p>

<p>2- Go to the root directory of the project named &ldquo;biomanufacturing&rdquo;. The &quot;Uppaal2ptaTranslator&quot; directory is a separate Java project which is used in our tool for pre/post processing purposes. This is a Maven project and you can install it by running the following commands in your terminal:</p>

<p>&nbsp; &nbsp; cd&nbsp; Uppaal2ptaTranslator<br />
&nbsp; &nbsp; mvn clean compile install<br />
&nbsp; &nbsp; cd ..<br />
&nbsp;<br />
3- Download and install Z3 SMT solver from&nbsp;<a href="https://github.com/Z3Prover/z3/releases">https://github.com/Z3Prover/z3/releases</a>&nbsp;(or comile and build it from scratch <a href="https://github.com/Z3Prover/z3">https://github.com/Z3Prover/z3</a>)<br />
&nbsp; - add the lib folder to path, or add it to the <strong>java.library.path</strong> in the code, or use <strong>java -Djava.library.path=...</strong><br />
<br />
4- From the root directory go to the &quot;pta-bioman&quot; directory which is the main module for translating timed-system processes into SMT codes. For compilation use the below&nbsp;commands in the command terminal:</p>

<p>&nbsp; &nbsp; cd pta-bioman<br />
&nbsp; &nbsp; cmake src<br />
&nbsp; &nbsp; make&nbsp;</p>

<p>5- For the Java project, three executable jar files will be produced as a result of successful installation in the &ldquo;target&rdquo; directory. The prefix &ldquo;uppaal-translator-1.0-SNAPSHOT&rdquo; is used for naming the jar files. The jar files that have postfix &ldquo;fat&rdquo; and &ldquo;jar-with-dependencies&rdquo; are self container. You can run them using the following command:</p>

<p>&nbsp; &nbsp; java -jar &lt;<em>jar-file-name</em>&gt; --upal2pta&nbsp;[uppaal XML file name]<br />
&nbsp; &nbsp; java -jar &lt;<em>jar-file-name</em>&gt; --show-uppaal-trace&nbsp;[uppaal generated trace file name (text file)]<br />
&nbsp; &nbsp; java -jar &lt;<em>jar-file-name</em>&gt; --smt2pta [--iterative[&lt;--internalOptimizer&gt;|&lt;--bisectionOptimizer&gt;] min max] [min_cost max_cost threshold] [SMT code produced&nbsp;file name] [[-]process names separated by white space ([-] as prefix means processes are excluded from optimization)]<br />
&nbsp; &nbsp; pta-bioman --ccs-smt [bound] [[-]process 1] [[-]target 1] ... [[-]process n] [[-]target n] ([-] as prefix of process name means to exlude from cost expression generation, and [-] as prefix of target means it is not considered as a target)<br />
&nbsp; &nbsp; pta-bioman --ccs-dreal&nbsp;[bound] [[-]process 1] [[-]target 1] ... [[-]process n] [[-]target n]</p>

<p>6- The SMT translation of the processes from in the xml file will be stored in text files named &ldquo;code.smt2&ldquo; . In addition to the SMT code, for each given process, a text file named as the input file with &quot;.txt&quot; extension will be generated that are used for the postprocessing purposes. Also, in the case of running the jar files with --upal2pta the single xml file will be translated to multiple single process container xml file in the format that our code accepts. If the --smt2pta is used then the resulting model will be saved in a text file named &quot;<a href="https://app.assembla.com/spaces/bio-manufacturing/git/source/master/benchmarking/OLD/linear-2dev-2prod-2clean/ResultModel.txt">ModelResult</a>.txt&quot;.</p>


<h2 style="font-style:italic"><big>An Example of Bio-manufacturing Scheduling Problem in UPPAAL CORA and CEPTA2SMT</big></h2>
In this example we modeled a sample bio-manufacturing scheduling problem in&nbsp;the form of priced timed autiomata (PTA). We modeled the problem in UPPAAL CORA and solved it using the CORA and our developed tool CEPTA2SMT. In the following, we explained and presented how to solve the problem by CEPTA2SMT step by step.&nbsp; The <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/Experiments-CASE2019/Models/bioman-linear-compact-3-3-2-1-CORA-ND.xml">source file</a> for the example is available online in the tools&#39; repository.<br />
&nbsp;
<h3 style="color:#aaaaaa; font-style:italic">UPPAAL Declarations</h3>
Our example is about a bio-manufacting scheduling problem in which some batches of materials need to become a final product using some upstream and downstreen devices. In our model, we have four different processes named <a href="/wiki/show/bio-manufacturing/ProductA1">ProductA1</a>, Bioreactor1, Purifier1, and Cleaner 1. <a href="/wiki/show/bio-manufacturing/ProductA1">ProductA1</a> represents the main production line. The upstpream and downstream devices are represented by Bioreactor1 and Purifier1 instance processes, respectively. The Cleaner1 represents a cleaner device that makes devices ready for a new batch processing. All the processes and their declarations are presented in the following.<br />
&nbsp;<br />
<strong>Global declarations:</strong>

<hr />int GPFID = 1;//global product fermenter ID<br />
int GFID = 0;//global fermenter ID<br />
int GPUID = 1;//global product purifier ID<br />
int GUID = 0;//global purifier ID
<hr /><br />
<strong><a href="/wiki/show/bio-manufacturing/ProductA1">ProductA1</a>&#39;s declarations:</strong>

<hr />clock cA;<br />
const int t11ss = 10; //setup bio time<br />
const int t11sf1 = 10; // delay between setup bio and fermentation<br />
const int t11f1 = 40; // fermentation time<br />
const int t11f1c = 20; &nbsp;//This is now a maximum time the max yield cells are alive before they all die. &nbsp;After this the system is infeasbile<br />
const int tFermDone = 40;&nbsp;<br />
const int tStartDying = 42;&nbsp;<br />
const int tEarly= 20;<br />
const int tAllDead = 200;// t11f1 + t11f1c + 40 -&gt; DELAY_ALL_DEAD<br />
const int t21f2 = 10; // purification time<br />
int PID = 0;
<hr /><br />
<strong>Bioreactor1&#39;s declarations:</strong>

<hr />clock cB;<br />
const int t11ss = 10;<br />
const int t11 = 10;<br />
int FID = 0;
<hr /><br />
<strong>Purifier1&#39;s declarations:</strong>

<hr />clock cP;<br />
const int t21ss = 10;<br />
const int t21 = 10;<br />
int UID = 0;
<hr /><br />
<strong>Cleaner1&#39;s declarations:</strong>

<hr />clock cC;<br />
const int t11 = 10;<br />
const int t21 = 10;
<hr /><br />
<strong>UPPAAL System Declarations:</strong>

<hr />chan &nbsp;sFerm1, &nbsp;eFerm1;<br />
chan &nbsp;sPur1, &nbsp;ePur1;<br />
chan &nbsp;cleanBio1, &nbsp;cleanPur1;<br />
<br />
<a href="/wiki/show/bio-manufacturing/ProdA1">ProdA1</a> = <a href="/wiki/show/bio-manufacturing/ProductA1">ProductA1</a>( &nbsp;sFerm1, &nbsp;eFerm1, sPur1 , &nbsp;ePur1 &nbsp;);<br />
<a href="/wiki/show/bio-manufacturing/ProdA2">ProdA2</a> = <a href="/wiki/show/bio-manufacturing/ProductA1">ProductA1</a>( &nbsp;sFerm1, &nbsp;eFerm1, sPur1 , &nbsp;ePur1 &nbsp;);<br />
<a href="/wiki/show/bio-manufacturing/ProdA3">ProdA3</a> = <a href="/wiki/show/bio-manufacturing/ProductA1">ProductA1</a>( &nbsp;sFerm1, &nbsp;eFerm1, sPur1 , &nbsp;ePur1 &nbsp;);<br />
&nbsp; &nbsp; &nbsp;&nbsp;<br />
Bio11 = Bioreactor1( &nbsp;sFerm1, &nbsp;eFerm1, &nbsp;cleanBio1 &nbsp;);<br />
Bio12 = Bioreactor1( &nbsp;sFerm1, &nbsp;eFerm1, &nbsp;cleanBio1 &nbsp;);<br />
Bio13 = Bioreactor1( &nbsp;sFerm1, &nbsp;eFerm1, &nbsp;cleanBio1 &nbsp;);<br />
<br />
Pur1 = Purifier1( &nbsp;sPur1, &nbsp;ePur1, &nbsp;cleanPur1 &nbsp;);<br />
Pur2 = Purifier1( &nbsp;sPur1, &nbsp;ePur1, &nbsp;cleanPur1 &nbsp;);<br />
<br />
Clean1 = Cleaner1( &nbsp;cleanBio1, cleanPur1 &nbsp;);<br />
<br />
system <a href="/wiki/show/bio-manufacturing/ProdA1">ProdA1</a>, <a href="/wiki/show/bio-manufacturing/ProdA2">ProdA2</a>, <a href="/wiki/show/bio-manufacturing/ProdA3">ProdA3</a>, Bio11, Bio12, Bio13, Pur1, Pur2, Clean1 ;

<hr /><br />
The below is&nbsp; a visualiztion of a configuration for the above problem with 3 batches of materials, 3 upstream devices, 2 downstram devices, and a cleaner.<br />
<img alt="" src="https://gitlab.com/vnv-tools/CEPTA2SMT/-/raw/main/Documents/images/uppaal-cora-model.png" /><br />
<strong>UPPAAL CORA Sample Schedule</strong><br />
<br />
The beow is a <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/wiki_example/CORA-Gant-Chart-3-3-2-1.xtr">Gant chart</a> that illustrates a sample optimal schedule for the <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/Experiments-CASE2019/Models/bioman-linear-compact-3-3-2-1-CORA.xml">deterministic version</a> of the above problem setting and a <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/Experiments-CASE2019/Models/bioman-linear-compact-3-CORA.q">sample query.</a>&nbsp;<br />
<br />
<img alt="" src="https://gitlab.com/vnv-tools/CEPTA2SMT/-/raw/main/Documents/images/gant-chart.png" /><br />
The cost function that is used for fermanting process in our example is shown in the below figure. The cost of producing new materials decreases by time until the time that the fermenting process is done, and then the cost increases by time untill all the grwoing cells die.&nbsp;<br />
<br />
<img alt="" src="https://gitlab.com/vnv-tools/CEPTA2SMT/-/raw/main/Documents/images/cost-func.png" />
<h3 style="color:#aaaaaa; font-style:italic">Runing the Example in CEPTA2SMT</h3>
We ran the example by our tool in a Windows environment. Next, we present a step-by-step manual about how to run the example.<br />
<br />
<em>Step 1) preprocessing the UPPAAL models and translate them into an XML format:</em>

<pre>
java -jar .\uppaal-translator-1.0-SNAPSHOT-fat.jar --upal2pta .\<a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/Experiments-CASE2019/Models/bioman-linear-compact-3-3-2-1-SMT-ND.xml">bioman-linear-compact-3-3-2-1-SMT-ND.xml</a></pre>
<br />
As a result of successfull execution of the above command script, a series of files will be generated that are listed below (all the generated files are available <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/wiki_example">here</a>):<br />
<a href="/wiki/show/bio-manufacturing/ProdA1">ProdA1</a>.xml&nbsp;<a href="/wiki/show/bio-manufacturing/ProdA2">ProdA2</a>.xml&nbsp;<a href="/wiki/show/bio-manufacturing/ProdA3">ProdA3</a>.xml<br />
Bio11.xml Bio12.xml Bio13.xml<br />
Pur1.xml Pur2.xml&nbsp;<br />
Clean1.xml<br />
<br />
Each of the above files represents an extended PTA. For example <a href="/wiki/show/bio-manufacturing/ProdA1">ProdA1</a><a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/wiki_example/ProdA1.xml">.xml</a> is a sample extended PTA that represents a production process.<br />
<br />
Step 2) Translating extended PTAs into SMT code, and generate solver-util files:
<pre>
.\bioman --ccs-smt 40 ProdA1 ENDJOB ProdA2 ENDJOB ProdA3 ENDJOB -Bio11 -wait -Bio12 -wait -Bio13 -wait -Pur1 -wait -Pur2 -wait -Clean1 -wait</pre>
<br />
As a result of successfull execution of the above command script, a series of files will be generated along with an SMT code file. All the generated files are listed below (all the generated files are available <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/wiki_example">here</a>):<br />
code.smt2<br />
solver.txt<br />
<a href="/wiki/show/bio-manufacturing/ProdA1">ProdA1</a>.xml.txt&nbsp;<a href="/wiki/show/bio-manufacturing/ProdA2">ProdA2</a>.xml.txt&nbsp;<a href="/wiki/show/bio-manufacturing/ProdA3">ProdA3</a>.xml.txt<br />
Bio11.xml.txt Bio12.xml.txt Bio13.xml.txt<br />
Pur1.xml.txt Pur2.xml.txt&nbsp;<br />
Clean1.xml.txt<br />
<br />
Step 3) Runing the SMT solver to find sample schedules for the above example:
<pre>
java -jar .\uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta --iterative 10 40 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Pur2 -Clean1</pre>
<br />
Here is the result of runing the above script command:
<hr />Reading from SMT file: code.smt2<br />
using process: <a href="/wiki/show/bio-manufacturing/ProdA1">ProdA1</a><br />
using process: <a href="/wiki/show/bio-manufacturing/ProdA2">ProdA2</a><br />
using process: <a href="/wiki/show/bio-manufacturing/ProdA3">ProdA3</a><br />
Reading solver&#39;s configurations from solver.txt ...<br />
&gt;&gt;&gt; Model has Real optimization declarations.<br />
SMT iterative code is segmented.<br />
Finding solution in the range of [10,40] steps...<br />
[10,40] running solver...<br />
Iteration: 25 -&gt; SATISFIABLE -&gt; 12600 ms<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_25">ProdA1_price_25</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;90<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_25">ProdA2_price_25</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;190<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_25">ProdA3_price_25</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
[10,24] running solver...<br />
Iteration: 17 -&gt; UNSATISFIABLE -&gt; 14503 ms<br />
[18,24] running solver...<br />
Iteration: 21 -&gt; SATISFIABLE -&gt; 2813 ms<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_21">ProdA1_price_21</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_21">ProdA2_price_21</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;90<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_21">ProdA3_price_21</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
[18,20] running solver...<br />
Iteration: 19 -&gt; SATISFIABLE -&gt; 8642 ms<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_19">ProdA1_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;240<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_19">ProdA2_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_19">ProdA3_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
[18,18] running solver...<br />
Iteration: 18 -&gt; SATISFIABLE -&gt; 7742 ms<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_18">ProdA1_price_18</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;190<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_18">ProdA2_price_18</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;80<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_18">ProdA3_price_18</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;60<br />
Minimum number of steps: 18<br />
sample used SMT code saved as test.smt2<br />
SATISFIABLE<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_18">ProdA1_price_18</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;190<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_18">ProdA2_price_18</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;80<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_18">ProdA3_price_18</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;60<br />
Total sum: 330.0<br />
number of functions: 2410<br />
Total used function definitions: 957<br />
<br />
<br />
=========================================<br />
<br />
<br />
&gt;&gt;&gt;Preparing and executing time by Z3 SMT solver: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 46748 mil sec
<hr />Step 4) Runing the SMT solver to find an optimal schedule:
<pre>
java -jar .\uppaal-translator-1.0-SNAPSHOT-fat.jar --smt2pta --iterative--internalOptimizer 19 19 code.smt2 ProdA1 ProdA2 ProdA3 -Bio11 -Bio12 -Bio13 -Pur1 -Pur2 -Clean1</pre>
<br />
Here is the result of runing the above script command:
<hr />Reading from SMT file: code.smt2<br />
using process: <a href="/wiki/show/bio-manufacturing/ProdA1">ProdA1</a><br />
using process: <a href="/wiki/show/bio-manufacturing/ProdA2">ProdA2</a><br />
using process: <a href="/wiki/show/bio-manufacturing/ProdA3">ProdA3</a><br />
Reading solver&#39;s configurations from solver.txt ...<br />
&gt;&gt;&gt; Model has Real optimization declarations.<br />
SMT iterative code is segmented.<br />
Finding solution in the range of [19,19] steps...<br />
[19,19] running solver...<br />
Iteration: 19 -&gt; SATISFIABLE -&gt; 8177 ms<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_19">ProdA1_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;240<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_19">ProdA2_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_19">ProdA3_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
Minimum number of steps: 19<br />
sample used SMT code saved as test.smt2<br />
Optimization is activated.<br />
Z3 time out set to: 6000000<br />
SATISFIABLE<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_19">ProdA1_price_19</a>= 40<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_19">ProdA2_price_19</a>= 40<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_19">ProdA3_price_19</a>= 40<br />
Total sum: 120.0<br />
SATISFIABLE<br />
<a href="/wiki/show/bio-manufacturing/ProdA1_price_19">ProdA1_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
<a href="/wiki/show/bio-manufacturing/ProdA2_price_19">ProdA2_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
<a href="/wiki/show/bio-manufacturing/ProdA3_price_19">ProdA3_price_19</a>: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;40<br />
Total sum: 120.0<br />
number of functions: 2542<br />
Total used function definitions: 1002<br />
<br />
<br />
=========================================<br />
<br />
<br />
&gt;&gt;&gt;Preparing and executing time by Z3 SMT solver: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 33036 mil sec
<hr />An optimal trace that is generated as a result of executing the above command is captured in <a href="https://cpslab.assembla.com/spaces/bio-manufacturing/git/source/master/wiki_example/ResultModel.txt">ResultModel.txt</a>.<br />
<br />
<br />
<br />
<br />
&nbsp;