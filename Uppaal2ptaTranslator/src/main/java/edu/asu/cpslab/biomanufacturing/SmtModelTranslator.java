package edu.asu.cpslab.biomanufacturing;


import com.microsoft.z3.*;
import com.microsoft.z3.enumerations.Z3_lbool;

import java.io.*;
import java.util.*;

/**
 * Created by mhekmatnejad on 9/20/18.
 * <p>
 * <p>
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class SmtModelTranslator {

    public static final Boolean PRINT_DEBUG_INFO = false;
    public static Boolean USE_REAL_OPTIMIZATION_EXPRESSIONS = true;

    public static Boolean PRICE_OPTIMIZATION_ACTIVE = true;
    public static Boolean USE_BISECTION_OPTIMIZATION = true;

    public static int Z3_TIME_OUT = 2000;//milliseconds
    public static int Z3_OPTIMIZATION_TIME_OUT = 60000;//milliseconds
    public static int Z3_TIME_OUT_INCREASE = 2000;//milliseconds
    public static int MAX_Z3_BISECTION_TIME_OUT = 20000;//milliseconds

    public Solver solver = null;
    public HashMap<String, String> cfg = new HashMap<String, String>();
    public com.microsoft.z3.Context ctx = null;
    //public Expr expResult;
    public BoolExpr[] expResult;
    public Status status = Status.UNKNOWN;
    Params z3Params = null;
    private boolean foundSatModel = false;
    private float satLowerBound;
    private float satLUpperBound;
    public boolean MONOTONICALLY_INCREASING = false;//assumption for the cost functions in optimization


    public String runZ3Solver(String filename, boolean isFile, boolean writeToFile, boolean solveIterative,
                              int startIt, int endIt, boolean internalOptimize, boolean bisectionOptimize,
                              float minCost, float maxCost, float epsilon) {

        // Store current System.out before assigning a new value
        PrintStream console = System.out;

        String result = "unsatisfiable";
        PRICE_OPTIMIZATION_ACTIVE = internalOptimize || bisectionOptimize;
        USE_BISECTION_OPTIMIZATION = bisectionOptimize;

        if (PRINT_DEBUG_INFO)
            System.out.println("SMT2 file: " + filename);
        Date before = new Date();

        System.gc();

 /*
  auto_config (bool) (default: true)
  debug_ref_count (bool) (default: false)
  dump_models (bool) (default: false)
  model (bool) (default: true)
  model_validate (bool) (default: false)
  proof (bool) (default: false)
  rlimit (unsigned int) (default: 0)
  smtlib2_compliant (bool) (default: false)
  timeout (unsigned int) (default: 4294967295)
  trace (bool) (default: false)
  trace_file_name (string) (default: z3.log)
  type_check (bool) (default: true)
  unsat_core (bool) (default: false)
  well_sorted_check (bool) (default: false)
 */
            //HashMap<String, String> cfg = new HashMap<String, String>();
            cfg.put("model", "true");
            if(solverMap.containsKey("Z3_TIME_OUT"))
                Z3_TIME_OUT = Integer.parseInt(solverMap.get("Z3_TIME_OUT"));
            if(solverMap.containsKey("Z3_OPTIMIZATION_TIME_OUT"))
                Z3_OPTIMIZATION_TIME_OUT = Integer.parseInt(solverMap.get("Z3_OPTIMIZATION_TIME_OUT"));
            if(solverMap.containsKey("Z3_TIME_OUT_INCREASE"))
                Z3_TIME_OUT_INCREASE = Integer.parseInt(solverMap.get("Z3_TIME_OUT_INCREASE"));
            if(solverMap.containsKey("MAX_Z3_BISECTION_TIME_OUT"))
                MAX_Z3_BISECTION_TIME_OUT = Integer.parseInt(solverMap.get("MAX_Z3_BISECTION_TIME_OUT"));

            if(USE_BISECTION_OPTIMIZATION && !solveIterative)
                cfg.put("timeout", String.valueOf(Z3_TIME_OUT));
            else
                cfg.put("timeout", String.valueOf(Z3_OPTIMIZATION_TIME_OUT));

            //cfg.put("smtlib2_compliant","true");//to be check in future
            //cfg.put("proof", "true");
            //cfg.put("unsat_core", "true");
            //cfg.put("opt.priority","pareto");//does not exist!!!
            //com.microsoft.z3.Context ctx = new com.microsoft.z3.Context(cfg);
            ctx = new com.microsoft.z3.Context(cfg);
            Model model = null;
            BoolExpr[] satExpResult = null;
            String smtCodeItr = "";
            String satSmtCodeItr = "";
            int satBound = Integer.parseInt(solverMap.get("MAX_STEP"));

            if(solveIterative)
            {
                System.out.println("Finding solution in the range of ["+startIt+","+endIt+"] steps...");
                if(endIt > orgReachabilityBound){
                    System.out.println("endIteration: <"+endIt+"> is greater than the given upperbound: <"+orgReachabilityBound+">" );
                    System.exit(0);
                }


                int itBound = startIt;
                int oldSatisfiableBound = -1;
                boolean singleStep = false;
                if(startIt == endIt)
                    singleStep = true;
                boolean maxTimeoutReached = false;
                int newTimeOut = Z3_TIME_OUT;// - Z3_TIME_OUT_INCREASE;
                z3Params = ctx.mkParams();//not used anymore

                while(startIt <= endIt && !maxTimeoutReached)
                {
                    itBound = (startIt + endIt) / 2;

                    //if(itBound == startIt) {
                    //    itBound = endIt;
                    //    startIt = endIt;
                    //}
                    getSmtCode(itBound);
                    smtCodeItr = _smtCode + smtReachabilityConstraint.
                                    replaceAll("_s_"+String.valueOf(orgReachabilityBound),
                                    "_s_"+String.valueOf(itBound));
                    expResult = ctx.parseSMTLIB2String(smtCodeItr, null, null, null, null);

                    if(itBound == oldSatisfiableBound && !singleStep){
                        status = Status.SATISFIABLE;
                        System.out.println("*Iteration: " + itBound + " -> " + status);
                        break;
                    }

                    solver = ctx.mkSolver();//("NRA");//LIA http://smtlib.cs.uiowa.edu/logics.shtml
                    //z3Params = ctx.mkParams();
                    //solver.add((BoolExpr) expResult);
                    for(BoolExpr bx: expResult)
                        solver.add(bx);
                    solver.setParameters(z3Params);
                    System.out.println("["+startIt+","+endIt+"]"+" running solver...");
                    Date solverStartTime = new Date();
                    status = solver.check();
                    long solvingTime = (new Date()).getTime() - solverStartTime.getTime();
                    System.out.println("Iteration: " + itBound + " -> " + status + " -> " + solvingTime + " ms");
                    if (status.equals(Status.SATISFIABLE)) {//1
                        endIt = itBound-1;
                        oldSatisfiableBound = itBound;
                        satBound = itBound;
                        satSmtCodeItr = smtCodeItr;

                        //print sample prices
                        model = solver.getModel();
                        satExpResult = expResult;

                        String[] optSymbols = new String[num_processesToOptimize];
                        for(int i = 0; i< num_processesToOptimize; i++){
                            optSymbols[i] = lstPrcNameToOptimize.get(i)+"_price_"+satBound;
                        }
                        int constraintNum = optSymbols.length;
                        RealExpr priceExprsReal[] = new RealExpr[constraintNum];
                        IntExpr priceExprsInt[] = new IntExpr[constraintNum];
                        if(USE_REAL_OPTIMIZATION_EXPRESSIONS) {
                            for (int i = 0; i < constraintNum; i++)
                                priceExprsReal[i] = ctx.mkRealConst(optSymbols[i]);
                            for (int i = 0; i < num_processesToOptimize; i++)
                                System.out.println(optSymbols[i] + ":   \t\t" + model.getConstInterp(priceExprsReal[i]));
                        }
                        else{
                            for (int i = 0; i < constraintNum; i++)
                                priceExprsInt[i] = ctx.mkIntConst(optSymbols[i]);
                            for (int i = 0; i < num_processesToOptimize; i++)
                                System.out.println(optSymbols[i] + ":   \t\t" + model.getConstInterp(priceExprsInt[i]));
                        }

                        if(startIt>endIt)
                            break;
                    }
                    else if(status.equals(Status.UNKNOWN) && solvingTime < newTimeOut){//2
                        System.out.println("Solver returned UNKNOWN");
                        if(USE_BISECTION_OPTIMIZATION){
                            System.out.println("Trying to explore lower time-steps...");
                            endIt = itBound-1;
                            oldSatisfiableBound = itBound;
                            satBound = itBound;
                            satSmtCodeItr = smtCodeItr;
                            if(startIt>endIt)
                                System.exit(0);

                        }else {
                            long t_diff = ((new Date()).getTime() - before.getTime());// / 1000;
                            System.out.println(">>>Preparing and executing time by Z3 SMT solver: \t\t" + t_diff + " mil sec");
                            System.exit(0);
                        }
                    }
                    else if ( startIt == endIt && status.equals(Status.UNKNOWN)){//3
                        newTimeOut += Z3_TIME_OUT_INCREASE;
                        if (newTimeOut > MAX_Z3_BISECTION_TIME_OUT ) {
                            System.out.println("could not find any satisfiable answer with the given configuration.");
                            System.exit(0);
                            break;
                        }
                        if (model == null) {
                            System.out.println(">>> increasing TIME_OUT to " + newTimeOut);
                            z3Params.add("timeout", newTimeOut);
                        }
                    }
                    else if((singleStep || (startIt == endIt && oldSatisfiableBound < 0)) &&
                            status.equals(Status.UNSATISFIABLE)){//4
                        System.setOut(console);
                        long t_diff = ((new Date()).getTime() - before.getTime());// / 1000;
                        System.out.println(">>>Preparing and executing time by Z3 SMT solver: \t\t" + t_diff + " mil sec");
                        result += "\n" + ">>>Preparing and executing time by Z3 SMT solver: \t\t" + t_diff + " mil sec";
                        return "UNSATISFIABLE";
                    }
                    else if((startIt == endIt && oldSatisfiableBound > 0) &&
                            status.equals(Status.UNSATISFIABLE)) {//5
                        status = Status.SATISFIABLE;
                        expResult = satExpResult;
                        itBound = oldSatisfiableBound;
                        smtCodeItr = satSmtCodeItr;
                        break;
                    }
                    else {//6
                        if(startIt == itBound)
                            startIt = endIt;
                        else
                            startIt = itBound+1;
                    }
                }//end while
                System.out.println("Minimum number of steps: "+itBound);
                //System.out.println(_smtCode);
                PrintStream ops = null;
                // Store current System.out before assigning a new value
                //PrintStream console = System.out;
                try {
                    System.out.println("sample used SMT code saved as test.smt2");
                    ops = new PrintStream(new File("test.smt2"));
                    System.setOut(ops);
                    System.out.println(smtCodeItr);
                    System.setOut(console);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    // Use stored value for output stream
                    System.setOut(console);
                }

            }else if (isFile)//not solve iterative
                expResult = ctx.parseSMTLIB2File(filename, null, null, null, null);
            else
                expResult = ctx.parseSMTLIB2String(filename, null, null, null, null);


            // Iterate over the formula.


            //Solver solver = ctx.mkSolver("LIA");//IDL http://smtlib.cs.uiowa.edu/logics.shtml
            solver = ctx.mkSolver();//("NRA");//LIA http://smtlib.cs.uiowa.edu/logics.shtml
            for(BoolExpr bx: expResult)
                solver.add(bx);
            //System.out.println(solver.getHelp());
            //Params params = ctx.mkParams();
            //params.add("logic","LIA");//IDL http://smtlib.cs.uiowa.edu/logics.shtml
            //System.out.println("Params:\n"+params);
            //solver.setParameters(params);
            //System.out.println("Logic:\n"+solver.getParameterDescriptions().getDocumentation(ctx.mkSymbol("logic")));
            //System.out.println("Kind:\n"+solver.getParameterDescriptions().getKind(ctx.mkSymbol("logic")));

            z3Params = ctx.mkParams();


            //---------------------------------------------------------//
            // Set constraints.
            //String[] optSymbols = {"ProductA_price_20", "Bioreactor_price_20", "Purifier_price_20", "Cleaner_price_20"};

            String[] optSymbols = new String[num_processesToOptimize];
            for(int i = 0; i< num_processesToOptimize; i++){
                optSymbols[i] = lstPrcNameToOptimize.get(i)+"_price_"+satBound;
            }
            int constraintNum = optSymbols.length;
            RealExpr priceExprsReal[] = new RealExpr[constraintNum];
            IntExpr priceExprsInt[] = new IntExpr[constraintNum];
            for (int i = 0; i < constraintNum; i++)
                priceExprsReal[i] = ctx.mkRealConst(optSymbols[i]);
            for (int i = 0; i < constraintNum; i++)
                priceExprsInt[i] = ctx.mkIntConst(optSymbols[i]);

            if (PRICE_OPTIMIZATION_ACTIVE) {
                System.out.println("Optimization is activated.");

                if (USE_BISECTION_OPTIMIZATION) {

                    String[] bsOptSymbols = new String[num_processesToOptimize];
                    for(int i = 0; i< num_processesToOptimize; i++){
                        bsOptSymbols[i] = lstPrcNameToOptimize.get(i)+"_price_";
                    }

                    RealExpr bsPriceExprsReal[] = new RealExpr[constraintNum];
                    IntExpr bsPriceExprsInt[] = new IntExpr[constraintNum];
                    ArithExpr[] optExpr = new ArithExpr[satBound];
                    for(int bc=0; bc <satBound; bc++) {
                        if(USE_REAL_OPTIMIZATION_EXPRESSIONS) {
                            for (int i = 0; i < constraintNum; i++)
                                bsPriceExprsReal[i] = ctx.mkRealConst(bsOptSymbols[i] + String.valueOf(bc));
                            optExpr[bc] = bsPriceExprsReal[0];
                            for (int pc = 1; pc < num_processesToOptimize; pc++)
                                optExpr[bc] = ctx.mkAdd(optExpr[bc], bsPriceExprsReal[pc]);
                        }
                        else{
                            for (int i = 0; i < constraintNum; i++)
                                bsPriceExprsInt[i] = ctx.mkIntConst(bsOptSymbols[i] + String.valueOf(bc));
                            optExpr[bc] = bsPriceExprsInt[0];
                            for (int pc = 1; pc < num_processesToOptimize; pc++)
                                optExpr[bc] = ctx.mkAdd(optExpr[bc], bsPriceExprsInt[pc]);
                        }
                    }
                    boolean maxTimeoutReached = false;
                    int newTimeOut = Z3_TIME_OUT;// - Z3_TIME_OUT_INCREASE;
                    model = null;
                    status = Status.UNKNOWN;
                    while (status.equals(Status.UNKNOWN) && !maxTimeoutReached) {
                        model = getOptimalSatisfiableModel(optExpr, String.valueOf(minCost), String.valueOf(maxCost), epsilon);
                        //model = getOptimalSatisfiableModel(optExpr, "220.0", "240.0", 1.0f);
                        newTimeOut += Z3_TIME_OUT_INCREASE;
                        if (newTimeOut > MAX_Z3_BISECTION_TIME_OUT)
                            break;
                        if (model == null && status.equals(Status.UNKNOWN)) {
                            System.out.println(">>> increasing TIME_OUT to " + newTimeOut);
                            //z3Params.add("solver2_timeout", newTimeOut);
                            z3Params.add("timeout", newTimeOut);
                        }
                    }
                    if (model == null) {
                        System.out.println("No model could be found!");
                        System.setOut(console);
                        long t_diff = ((new Date()).getTime() - before.getTime());// / 1000;
                        System.out.println(">>>Preparing and executing time by Z3 SMT solver: \t\t" + t_diff + " mil sec");
                        result += "\n" + ">>>Preparing and executing time by Z3 SMT solver: \t\t" + t_diff + " mil sec";

                        return "FAILED!";
                    }
                } else {//Internal Z3 optimizer
                    Optimize opt = ctx.mkOptimize();
                    System.out.println("Z3 time out set to: "+Z3_OPTIMIZATION_TIME_OUT);
                    z3Params.add("timeout", Z3_OPTIMIZATION_TIME_OUT);
                    opt.setParameters(z3Params);
                    //Params optParams = opt.pa
                    //opt.setParameters();
                    //opt.getParameterDescriptions();
                    //(optsmt_engine, maxsat_engine, priority, dump_benchmarks, timeout, rlimit, enable_sls, enable_sat,
                    // elim_01, pp.neat, pb.compile_equality, maxres.hill_climb, maxres.add_upper_bound_block,
                    // maxres.max_num_cores, maxres.max_core_size, maxres.maximize_assignment, maxres.max_correction_set_size,
                    // maxres.wmax, maxres.pivot_on_correction_set)

                    for(BoolExpr bx: expResult)
                        opt.Add(bx);
                    // Set objectives.
                    Optimize.Handle optHdls[] = new Optimize.Handle[constraintNum];
                    if(USE_REAL_OPTIMIZATION_EXPRESSIONS) {
                        for (int i = 0; i < constraintNum; i++)
                            optHdls[i] = opt.MkMinimize(priceExprsReal[i]);
                    }
                    else {
                        for (int i = 0; i < constraintNum; i++)
                            optHdls[i] = opt.MkMinimize(priceExprsInt[i]);
                    }

                    status = opt.Check();
                    System.out.println(status);
                    if(status == Status.SATISFIABLE) {
                        float sumPrice = 0;
                        for (int i = 0; i < constraintNum; i++) {
                            System.out.println(optSymbols[i] + "= " + optHdls[i]);
                            if (optHdls[i].getValue().isReal())
                                sumPrice += Float.parseFloat(optHdls[i].getValue().toString());
                            else if (optHdls[i].getValue().isInt())
                                sumPrice += Integer.parseInt(optHdls[i].getValue().toString());
                        }
                        System.out.println("Total sum: " + sumPrice);
                    }
                    model = opt.getModel();
                }
            }
            //---------------------------------------------------------//


            if (!PRICE_OPTIMIZATION_ACTIVE && (model==null || status!=Status.SATISFIABLE)) {
                System.out.println("Running SMT solver using MAX_STEP: <"+satBound+"> Z3_TIME_OUT: <"+Z3_TIME_OUT+">");
                status = solver.check();
            }
            //Status status = solver.check();
            long t_diff = ((new Date()).getTime() - before.getTime());// / 1000;


            // Creating a File object that represents the disk file.
            PrintStream o = null;

            try {
                o = new PrintStream(new File("ResultModel.txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                // Use stored value for output stream
                System.setOut(console);
            }

            //if(PRINT_DEBUG_INFO)
            System.out.println(status);
            //if(PRINT_DEBUG_INFO)
            Map<String, ProcessRunningData> prcMap = new HashMap<String, ProcessRunningData>();
            if (status.equals(Status.SATISFIABLE)) {
                int counter = 0;

                //--------------------------------//
                //Model model = solver.getModel();
                if (!PRICE_OPTIMIZATION_ACTIVE && (model==null || status!=Status.SATISFIABLE))
                    model = solver.getModel();

                if (true) {
                    float sumPrice = 0;
                    if(USE_REAL_OPTIMIZATION_EXPRESSIONS)
                        for (int i = 0; i < constraintNum; i++) {
                            System.out.println(optSymbols[i] + ":   \t\t" + model.getConstInterp(priceExprsReal[i]));
                            if(model.getConstInterp(priceExprsReal[i]).isReal() || model.getConstInterp(priceExprsReal[i]).isInt()) {
                                String str_val = model.getConstInterp(priceExprsReal[i]).toString();
                                Float f_val = 0f;
                                if(str_val.contains("/")) {
                                    String lr[] = str_val.split("/");
                                    if(!lr[0].contains("."))
                                        lr[0] += ".0";
                                    if(!lr[1].contains("."))
                                        lr[1] += ".0";
                                    f_val = Float.parseFloat(lr[0]) / Float.parseFloat(lr[1]);
                                }
                                else
                                    f_val = Float.parseFloat(model.getConstInterp(priceExprsReal[i]).toString());
                                sumPrice += f_val;
                            }
                        }
                    else
                        for (int i = 0; i < constraintNum; i++) {
                            System.out.println(optSymbols[i] + ":   \t\t" + model.getConstInterp(priceExprsInt[i]));
                            if(model.getConstInterp(priceExprsInt[i]).isReal() || model.getConstInterp(priceExprsInt[i]).isInt())
                                sumPrice += Float.parseFloat(model.getConstInterp(priceExprsInt[i]).toString());
                        }
                    System.out.println("Total sum: "+sumPrice);
                }
                //if(PRINT_DEBUG_INFO)
                //    System.out.println("model: \n"+model);
                result += "\n" + model.toString();
                for (int i = 0; i < model.getFuncDecls().length; i++)
                    System.out.println("value: \n" + model.getFuncInterp(model.getFuncDecls()[i]));
                int fun_num = model.getConstDecls().length;
                System.out.println("number of functions: " + fun_num);
                FuncDecl funcs[] = model.getConstDecls();
                for (int i = 0; i < fun_num; i++) {
                    FuncDecl fun = funcs[i];
                    Expr val_expr = model.getConstInterp(fun);
                    //filtering out the negative, zero and false values from the data
                    //todo: maybe in future we need all values disregarding of their sign and being false
                    if ((val_expr.isBool() && val_expr.getBoolValue().equals(Z3_lbool.Z3_L_TRUE))
                            || (val_expr.isReal())// && Double.parseDouble(val_expr.toString())>0)//todo: we have to ignore negative numbers in future
                            || (val_expr.isInt() && Integer.parseInt(val_expr.toString()) > 0)
                            || (val_expr.isBV() && ((BitVecNum) val_expr).getInt() >= 0)
                            ) {
                        String tokens[] = fun.getName().toString().split("_");
                        if (tokens.length == 3) {
                            switch (tokens[1].codePointAt(0)) {
                                case 's':
                                    if (PRINT_DEBUG_INFO)
                                        System.out.println("State");
                                    if (prcMap.get(tokens[0]) == null)
                                        prcMap.put(tokens[0], new ProcessRunningData(tokens[0]));
                                    if(val_expr.isInt()) {
                                        tokens[1] = orgNameMap.get(tokens[0]).get("s." + val_expr.toString());
                                        prcMap.get(tokens[0]).staMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), Integer.parseInt(val_expr.toString()));
                                    }
                                    else{
                                        tokens[1] = orgNameMap.get(tokens[0]).get("s." + ((BitVecNum) val_expr).getInt());
                                        prcMap.get(tokens[0]).staMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), ((BitVecNum) val_expr).getInt());
                                    }
                                    break;
                                case 'A':
                                    if (PRINT_DEBUG_INFO)
                                        System.out.println("Action");
                                    if (prcMap.get(tokens[0]) == null)
                                        prcMap.put(tokens[0], new ProcessRunningData(tokens[0]));
                                    tokens[1] = orgNameMap.get(tokens[0]).get("A." + tokens[1].substring(1, tokens[1].length()));
                                    prcMap.get(tokens[0]).msgMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), true);
                                    break;
                                case 'T':
                                    if (PRINT_DEBUG_INFO)
                                        System.out.println("Transition");
                                    if (tokens[1].equals("Tnull")) {
                                        if (prcMap.get(tokens[0]) == null)
                                            prcMap.put(tokens[0], new ProcessRunningData(tokens[0]));
                                        prcMap.get(tokens[0]).trsMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), true);
                                    } else if (tokens[1].equals("Td")) {
                                        if (prcMap.get(tokens[0]) == null)
                                            prcMap.put(tokens[0], new ProcessRunningData(tokens[0]));
                                        prcMap.get(tokens[0]).trsMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), true);
                                    } else//regular transition
                                    {
                                        if (prcMap.get(tokens[0]) == null)
                                            prcMap.put(tokens[0], new ProcessRunningData(tokens[0]));
                                        tokens[1] = orgNameMap.get(tokens[0]).get("T." + tokens[1].substring(1, tokens[1].length()));
                                        prcMap.get(tokens[0]).trsMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), true);
                                    }
                                    break;
                                case 'D':
                                    if (PRINT_DEBUG_INFO)
                                        System.out.println("Delay");
                                    if (prcMap.get(tokens[0]) == null)
                                        prcMap.put(tokens[0], new ProcessRunningData(tokens[0]));
                                    Float delay = 0f;
                                    if(val_expr.toString().contains("/"))
                                        delay = Float.parseFloat(val_expr.toString().split("/")[0])/Float.parseFloat(val_expr.toString().split("/")[1]);
                                    else
                                        delay = Float.parseFloat(val_expr.toString());
                                    prcMap.get(tokens[0]).dlyMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), delay);
                                    break;
                                case 'C':
                                    if (PRINT_DEBUG_INFO)
                                        System.out.println("Clock");
                                    if (prcMap.get(tokens[0]) == null)
                                        prcMap.put(tokens[0], new ProcessRunningData(tokens[0]));
                                    Float clock = 0f;
                                    if(val_expr.toString().contains("/"))
                                        clock = Float.parseFloat(val_expr.toString().split("/")[0])/Float.parseFloat(val_expr.toString().split("/")[1]);
                                    else
                                        clock = Float.parseFloat(val_expr.toString());
                                    prcMap.get(tokens[0]).clkMap.put(new Pair<Integer, String>(Integer.parseInt(tokens[2]), tokens[1]), Math.abs(clock));
                                    break;
                            }
                        } else if (tokens.length == 2 && tokens[0].equals("z")) {
                            //if(PRINT_DEBUG_INFO)
                            //    System.out.println("Global Clock");
                        }
                        if (PRINT_DEBUG_INFO)
                            System.out.println("var_name: <" + fun.getName().toString() + ">\t\t\t\t range_type: <" + fun.getRange().toString() + "> \t\tvalue: <" + val_expr + ">");
                        counter++;
                    }
                }
                System.out.println("Total used function definitions: " + counter);
                //reconstructing the event/action sequences
                System.out.println("\n\n=========================================\n\n");
                int numPrcs = prcMap.size();
                String prcNames[] = Arrays.copyOf(prcMap.keySet().toArray(), numPrcs, String[].class);
                //while(true)
                {
                    Iterator<Pair<Integer, String>> trsItrs[] = new Iterator[numPrcs];
                    Iterator<Pair<Integer, String>> dlyItrs[] = new Iterator[numPrcs];
                    Iterator<Pair<Integer, String>> clkItrs[] = new Iterator[numPrcs];
                    Iterator<Pair<Integer, String>> staItrs[] = new Iterator[numPrcs];
                    Iterator<Pair<Integer, String>> msgItrs[] = new Iterator[numPrcs];

                    for (int pc = 0; pc < numPrcs; pc++) {
                        trsItrs[pc] = prcMap.get(prcNames[pc]).trsMap.keySet().iterator();
                        dlyItrs[pc] = prcMap.get(prcNames[pc]).dlyMap.keySet().iterator();
                        clkItrs[pc] = prcMap.get(prcNames[pc]).clkMap.keySet().iterator();
                        staItrs[pc] = prcMap.get(prcNames[pc]).staMap.keySet().iterator();
                        msgItrs[pc] = prcMap.get(prcNames[pc]).msgMap.keySet().iterator();
                    }

                    int min_step = 0;
                    Pair<Integer, String> trsPairs[] = new Pair[numPrcs];
                    Pair<Integer, String> dlyPairs[] = new Pair[numPrcs];
                    Pair<Integer, String> clkPairs[] = new Pair[numPrcs];
                    Pair<Integer, String> staPairs[] = new Pair[numPrcs];
                    Pair<Integer, String> msgPairs[] = new Pair[numPrcs];
                    boolean trsEnded[] = new boolean[numPrcs];
                    boolean staEnded[] = new boolean[numPrcs];
                    boolean msgEnded[] = new boolean[numPrcs];
                    boolean clkEnded[] = new boolean[numPrcs];
                    boolean dlyEnded[] = new boolean[numPrcs];

                    for (int pc = 0; pc < numPrcs; pc++) {
                        trsEnded[pc] = false;
                        staEnded[pc] = false;
                        msgEnded[pc] = false;
                        clkEnded[pc] = false;
                        dlyEnded[pc] = false;
                        if (trsItrs[pc].hasNext())
                            trsPairs[pc] = trsItrs[pc].next();
                        if (dlyItrs[pc].hasNext())
                            dlyPairs[pc] = dlyItrs[pc].next();
                        if (clkItrs[pc].hasNext())
                            clkPairs[pc] = clkItrs[pc].next();
                        if (staItrs[pc].hasNext())
                            staPairs[pc] = staItrs[pc].next();
                        if (msgItrs[pc].hasNext())
                            msgPairs[pc] = msgItrs[pc].next();
                    }

                    boolean stop = false;
                    boolean printed = false;

                    // Assign o to output stream
                    if (writeToFile)
                        System.setOut(o);

                    while (!stop) {
                        stop = true;
                        printed = false;
                        System.out.println("\nSTEP < " + min_step + " >");
                        //transitions
                        for (int pc = 0; pc < numPrcs; pc++) {
                            if (trsPairs[pc]!=null && trsPairs[pc].first == min_step) {
                                //System.out.println(trsPairs[pc].first + " " + prcNames[pc] +"."+ trsPairs[pc].second + " " + prcMap.get(prcNames[pc]).trsMap.get(trsPairs[pc]));
                                System.out.println("TRANS: \t" + prcNames[pc] + ": " + trsPairs[pc].second);
                                printed = true;
                                if (trsItrs[pc].hasNext())
                                    trsPairs[pc] = trsItrs[pc].next();
                                else
                                    trsEnded[pc] = true;
                            }
                            stop = stop && trsEnded[pc];
                        }
                        //states
                        for (int pc = 0; pc < numPrcs; pc++) {
                            if (staPairs[pc]!=null && staPairs[pc].first == min_step) {
                                System.out.println("STATE: \t" + prcNames[pc] + "." + staPairs[pc].second);//+"("+prcMap.get(prcNames[pc]).staMap.get(staPairs[pc])+")" );
                                printed = true;
                                if (staItrs[pc].hasNext())
                                    staPairs[pc] = staItrs[pc].next();
                                else
                                    staEnded[pc] = true;
                            }
                        }
                        //Messages
                        for (int pc = 0; pc < numPrcs; pc++) {
                            if (msgPairs[pc]!=null && msgPairs[pc].first == min_step) {
                                System.out.println("MSG: \t" + prcNames[pc] + "." + msgPairs[pc].second);
                                printed = true;
                                if (msgItrs[pc].hasNext())
                                    msgPairs[pc] = msgItrs[pc].next();
                                else
                                    msgEnded[pc] = true;
                            }
                        }
                        //clocks
                        for (int pc = 0; pc < numPrcs; pc++) {
                            if (clkPairs[pc]!=null && clkPairs[pc]!=null && clkPairs[pc].first == min_step) {
                                System.out.println("CLOCK: \t" + prcNames[pc] + "." + clkPairs[pc].second + "(" + prcMap.get(prcNames[pc]).clkMap.get(clkPairs[pc]) + ")");
                                printed = true;
                                if (clkItrs[pc].hasNext())
                                    clkPairs[pc] = clkItrs[pc].next();
                                else
                                    clkEnded[pc] = true;
                            }
                        }
                        //delays
                        for (int pc = 0; pc < numPrcs; pc++) {
                            if (dlyPairs[pc]!=null && dlyPairs[pc].first == min_step) {
                                System.out.println("DELAY: \t" + prcNames[pc] + "." + dlyPairs[pc].second + "(" + prcMap.get(prcNames[pc]).dlyMap.get(dlyPairs[pc]) + ")");
                                printed = true;
                                if (dlyItrs[pc].hasNext())
                                    dlyPairs[pc] = dlyItrs[pc].next();
                                else
                                    dlyEnded[pc] = true;
                            }
                        }

                        if (printed)
                            System.out.println();
                        min_step++;
                    }
                }
            }
            System.setOut(console);
            System.out.println(">>>Preparing and executing time by Z3 SMT solver: \t\t" + t_diff + " mil sec");
            result += "\n" + ">>>Preparing and executing time by Z3 SMT solver: \t\t" + t_diff + " mil sec";

        return result;

    }


    public Model getOptimalSatisfiableModel(ArithExpr[] objectiveFuncExpr, String strLowerBound, String strUpperBound, float epsilon) {
        Solver optSolver = null;//ctx.mkSolver();//("NRA");
        //optSolver.add((BoolExpr) expResult);
        boolean successful = false;
        BoolExpr[] optExpr = new BoolExpr[objectiveFuncExpr.length];
        float lowerBound = Float.parseFloat(strLowerBound);
        float upperBound = Float.parseFloat(strUpperBound);
        //float oldLowerBound = lowerBound;
        float oldUpperBound = upperBound;

        if(foundSatModel){
            lowerBound = satLowerBound;
            upperBound = satLUpperBound;
            System.out.print("*");
        }

        System.out.println("Given interval = [" + lowerBound + " , " + upperBound + "]");

        upperBound = (lowerBound + upperBound) / 2.0f;

        while (!successful) {
            optSolver = ctx.mkSolver();
            optSolver.setParameters(z3Params);

            for(BoolExpr bx: expResult)
                optSolver.add(bx);
            //optSolver.add((BoolExpr) expResult);
            if(USE_REAL_OPTIMIZATION_EXPRESSIONS) {
                for (int i = 0; i < optExpr.length; i++)
                    optExpr[i] = ctx.mkLe(objectiveFuncExpr[i], ctx.mkReal(String.valueOf(upperBound)));
            }
            else{
                for (int i = 0; i < optExpr.length; i++)
                    optExpr[i] = ctx.mkLe(objectiveFuncExpr[i], ctx.mkInt(String.valueOf(upperBound)));
            }
            //if(lowerBound<upperBound)
            //    optExpr[optExpr.length-1] = ctx.mkAnd(ctx.mkGe(objectiveFuncExpr[optExpr.length-1], ctx.mkReal(String.valueOf(lowerBound))),
            //            ctx.mkLe(objectiveFuncExpr[optExpr.length-1], ctx.mkReal(String.valueOf(upperBound))));
            //else if(lowerBound==upperBound) {
            //    optExpr[optExpr.length-1] = ctx.mkEq(objectiveFuncExpr[optExpr.length-1], ctx.mkReal(String.valueOf(lowerBound)));
            //}
            if(MONOTONICALLY_INCREASING) {
                for (BoolExpr bx : optExpr)
                    optSolver.add(bx);
            }else{
                //optSolver.add(optExpr[optExpr.length-1]);
                optSolver.add( ctx.mkAnd(ctx.mkGe(objectiveFuncExpr[optExpr.length-1], ctx.mkReal(String.valueOf(lowerBound))),
                                ctx.mkLe(objectiveFuncExpr[optExpr.length-1], ctx.mkReal(String.valueOf(upperBound)))) );
            }

            System.out.println("Trying bisection [" + lowerBound + " , " + upperBound + "]");
            status = optSolver.check();
            if (status.equals(Status.SATISFIABLE)) {
                foundSatModel = true;
                satLowerBound = lowerBound;
                satLUpperBound = upperBound;
                System.out.println("< OK >");
                oldUpperBound = upperBound;
                upperBound = (lowerBound + upperBound) / 2.0f;
                if(PRINT_DEBUG_INFO) {
                    PrintStream ops = null;
                    // Store current System.out before assigning a new value
                    PrintStream console = System.out;
                    try {
                        ops = new PrintStream(new File("bisection.smt2"));
                        System.setOut(ops);
                        System.out.println(optSolver.toString());
                        System.setOut(console);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        // Use stored value for output stream
                        System.setOut(console);
                    }
                }
            } else {
                if (status.equals(Status.UNSATISFIABLE))
                    System.out.println("UNSAT");
                else
                    System.out.println("TIME OUT");
                lowerBound = upperBound;
                upperBound = oldUpperBound;
            }

            if ((upperBound - lowerBound) <= epsilon) {
                if (status.equals(Status.SATISFIABLE))
                    return optSolver.getModel();
                else
                    return null;
            }
        }

        return solver.getModel();
    }

    //===============================================//
    //===============================================//
    public static void main(String[] args) {

        SmtModelTranslator smtTrs = new SmtModelTranslator();
        //default inputs
        String smtFileName = "code.smt2";
        //String prcNames[] = {"ProductA", "Bioreactor", "Purifier", "Cleaner"};
        String prcNames[] = {"ProdA1", "ProdA2", "Bio11", "Bio12", "Pur1", "Clean1"};
        boolean solveIterative = false;
        int bias = 0;
        int startIt = 40;
        int endIt = 40;
        //float minCost=0f, maxCost=1000f, epsilon = 1f;
        float minCost=380f, maxCost=480f, epsilon = 1f;
        boolean internalOptimize = false;
        boolean bisectionOptimize = false;
        //read inputs from command line
        if (args.length > 1) {
            int itrBias = 0;
            if(args[1].startsWith("--iterative")) {
                solveIterative = true;
                startIt = Integer.parseInt(args[2]);
                endIt = Integer.parseInt(args[3]);
                if(startIt>endIt){
                    System.out.println("start iteration must be less than end iteration.");
                    System.exit(0);
                }
                bias += 3;
                itrBias = 2;
            }
            if(args[1].endsWith("--internalOptimizer"))
                internalOptimize = true;
            if(args[1].endsWith("--bisectionOptimizer")) {
                minCost = Float.parseFloat(args[2+itrBias]);
                maxCost = Float.parseFloat(args[3+itrBias]);
                epsilon = Float.parseFloat(args[4+itrBias]);
                if(solveIterative)
                    bias += 3;
                else
                    bias += 4;
                bisectionOptimize = true;
            }
            System.out.println("Reading from SMT file: " + args[1+bias]);
            smtFileName = args[bias+1];
            if (args.length > (2+bias)) {
                for (int c = (2+bias); c < args.length; c++) {
                    if(args[c].startsWith("-")){
                        lstPrcName.add(args[c].substring(1,args[c].length()));
                    }else {
                        lstPrcName.add(args[c]);
                        lstPrcNameToOptimize.add(args[c]);
                        System.out.println("using process: " + args[c]);
                    }
                }
            } else {
                for (int c = 0; c < prcNames.length; c++) {
                    if(prcNames[c].startsWith("-")){
                        lstPrcName.add(prcNames[c].substring(1,prcNames[c].length()));
                    }else {
                        lstPrcName.add(prcNames[c]);
                        lstPrcNameToOptimize.add(prcNames[c]);
                        System.out.println("using process: " + prcNames[c]);
                    }
                }
            }
        } else {
            for (int c = 0; c < prcNames.length; c++) {
                if(prcNames[c].startsWith("-")){
                    lstPrcName.add(prcNames[c].substring(1,prcNames[c].length()));
                }else {
                    lstPrcName.add(prcNames[c]);
                    lstPrcNameToOptimize.add(prcNames[c]);
                    System.out.println("using process: " + prcNames[c]);
                }
            }
        }
        num_processesToOptimize = lstPrcNameToOptimize.size();
        num_processes = lstPrcName.size();
        smtTrs.loadOriginalFileNames(lstPrcName);
        smtTrs.checkIfModelOptimizationIsReal(smtFileName);
        if(solveIterative)
            smtTrs.loadSmtIteratively(smtFileName);
        smtTrs.runZ3Solver(smtFileName, true, true, solveIterative, startIt, endIt,
                            internalOptimize, bisectionOptimize, minCost, maxCost, epsilon);
    }

    static class ProcessRunningData {

        public ProcessRunningData(String name) {
            prcName = name;
        }

        private String prcName = "";
        private SortedMap<Pair<Integer, String>, Integer> staMap = new TreeMap<Pair<Integer, String>, Integer>();
        private SortedMap<Pair<Integer, String>, Boolean> trsMap = new TreeMap<Pair<Integer, String>, Boolean>();
        private SortedMap<Pair<Integer, String>, Float> clkMap = new TreeMap<Pair<Integer, String>, Float>();
        private SortedMap<Pair<Integer, String>, Float> dlyMap = new TreeMap<Pair<Integer, String>, Float>();
        private SortedMap<Pair<Integer, String>, Boolean> msgMap = new TreeMap<Pair<Integer, String>, Boolean>();


        public void test() {
            trsMap.put(new Pair<Integer, String>(1, "a2"), true);
            trsMap.put(new Pair<Integer, String>(1, "a1"), true);
            trsMap.put(new Pair<Integer, String>(0, "a3"), true);
            trsMap.put(new Pair<Integer, String>(2, "a3"), true);
            Collections.synchronizedSortedMap(trsMap);
            for (Pair<Integer, String> pair : trsMap.keySet()) {
                System.out.println(pair.first + " " + pair.second + " " + trsMap.get(pair));
            }
        }
    }

    public static class Pair<F extends Comparable<F>, S extends Comparable<S>>
            implements Comparable<Pair<F, S>> {

        public final F first;
        public final S second;

        /**
         * Constructor for a Pair.
         *
         * @param first  the first object in the Pair
         * @param second the second object in the pair
         */
        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }

        /**
         * Checks the two objects for equality by delegating to their respective
         * {@link Object#equals(Object)} methods.
         *
         * @param o the {@link Pair} to which this one is to be checked for equality
         * @return true if the underlying objects of the Pair are both considered
         * equal
         */
        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Pair)) {
                return false;
            }
            Pair<?, ?> p = (Pair<?, ?>) o;
            return Objects.equals(p.first, first) && Objects.equals(p.second, second);
        }

        /**
         * Compute a hash code using the hash codes of the underlying objects
         *
         * @return a hashcode of the Pair
         */
        @Override
        public int hashCode() {
            return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode());
        }

        /**
         * Convenience method for creating an appropriately typed pair.
         *
         * @param a the first object in the Pair
         * @param b the second object in the pair
         * @return a Pair that is templatized with the types of a and b
         */
        public static <A extends Comparable<A>, B extends Comparable<B>> Pair<A, B> create(A a, B b) {
            return new Pair<A, B>(a, b);
        }

        @Override
        public int compareTo(Pair<F, S> that) {
            int cmp = this.first.compareTo(that.first);
            if (cmp == 0)
                cmp = this.second.compareTo(that.second);
            return cmp;
        }
    }

    public void loadOriginalFileNames(List<String> lstProcessName) {
        for (int i = 0; i < lstProcessName.size(); i++) {

            orgNameMap.put(lstProcessName.get(i), new HashMap<String, String>());

            File file = new File(lstProcessName.get(i) + ".xml.txt");

            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(file));
                String st;
                while ((st = br.readLine()) != null) {
                    if (PRINT_DEBUG_INFO)
                        System.out.println(st);
                    String tokens[] = st.split(" ");
                    if (tokens.length == 2 && tokens[0].startsWith("s.")) {
                        orgNameMap.get(lstProcessName.get(i)).put(tokens[0], tokens[1]);
                    } else if (tokens.length == 2 && tokens[0].startsWith("A.")) {
                        orgNameMap.get(lstProcessName.get(i)).put(tokens[0], tokens[1]);
                    } else if (tokens.length == 1 && tokens[0].startsWith("A.")) {
                        orgNameMap.get(lstProcessName.get(i)).put(tokens[0], "");
                    } else if (tokens.length == 4 && tokens[0].startsWith("T.")) {
                        orgNameMap.get(lstProcessName.get(i)).put(tokens[0], tokens[1] + " " + tokens[2] + " " + tokens[3]);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //read solver data from file
        File file = new File("solver.txt");
        System.out.println("Reading solver's configurations from solver.txt ...");
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                if (PRINT_DEBUG_INFO)
                    System.out.println(st);
                if(st.replaceAll(" ","").replaceAll("\n","").length()>0) {
                    String tokens[] = st.split("=");
                    if (tokens[1].contains("%"))
                        tokens[1] = tokens[1].substring(0, tokens[1].indexOf("%"));
                    solverMap.put(tokens[0].replaceAll(" ", ""), tokens[1].replaceAll(" ", ""));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("The solver data file \"solver.txt\" has wrong format.\n The correct format is <key>=<value>%comment");
        }
        if(Integer.parseInt(solverMap.get("NUM_PRC"))!= num_processes) {
            System.out.println(">>>>>ERROR the number of processes from the solver.txt is not the same as the given by arguments.");
            System.exit(0);
        }
    }

    public void loadSmtIteratively(String filename){

        File smtFile = new File(filename);
        if(!smtFile.exists()) {
            System.out.println(filename + " does not exist.");
            System.exit(0);
        }

        FileInputStream is = null;
        String strLine = "";
        String smtCode = "";
        smtReachabilityConstraint = "";
        boolean foundReachabilityConstraint = false;
        boolean endOfUseableCode = false;
        try {
            BufferedReader br = new BufferedReader(new FileReader(smtFile));
            strLine = br.readLine();
            int cnt = 0;
            while(strLine!=null && !endOfUseableCode){
                //System.out.println(strLine);
                if(strLine.startsWith(";;;===>>>Iteration:")){
                    if(cnt>0){
                        smtIterativeCode.add(cnt-1, smtCode);
                        smtCode = "";
                    }
                    cnt++;
                }else if(strLine.startsWith(";;;===>>>Reachability Constraint")) {
                    foundReachabilityConstraint = true;
                    orgReachabilityBound = cnt;
                }
                else if(foundReachabilityConstraint && strLine.startsWith(";;;===>>>")) {
                    foundReachabilityConstraint = false;
                    endOfUseableCode = true;
                }

                if(foundReachabilityConstraint)
                    smtReachabilityConstraint += strLine + "\n";
                if(!foundReachabilityConstraint && !endOfUseableCode)
                    smtCode += strLine + "\n";
                strLine = br.readLine();
            }
            if(smtCode.length()>0)
                smtIterativeCode.add(cnt-1, smtCode);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("SMT iterative code is segmented.");
    }

    public void checkIfModelOptimizationIsReal(String filename){

        File smtFile = new File(filename);
        if(!smtFile.exists()) {
            System.out.println(filename + " does not exist.");
            System.exit(0);
        }
        USE_REAL_OPTIMIZATION_EXPRESSIONS = false;
        FileInputStream is = null;
        String strLine = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(smtFile));
            strLine = br.readLine();
            int cnt = 0;
            while(strLine!=null && cnt<2){
                if(strLine.startsWith(";;;===>>>Iteration:"))
                    cnt++;
                if(strLine.contains("declare-const") && strLine.contains(" Real") && strLine.contains("_price_")) {
                    USE_REAL_OPTIMIZATION_EXPRESSIONS = true;
                    break;
                }
                strLine = br.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!USE_REAL_OPTIMIZATION_EXPRESSIONS)
            System.out.println(">>> Model does not have Real optimization declarations.");
        else
            System.out.println(">>> Model has Real optimization declarations.");
    }

    public void getSmtCode(int bound){
        int startItr = 0;
        if(_lastIteration<bound){
            startItr = _lastIteration;
        }
        else
            _smtCode = "";
        for(int itr = startItr; itr<bound; itr++){
            _smtCode += smtIterativeCode.get(itr) + "\n";
        }
        _lastIteration = bound;
    }

    String _smtCode = "";
    int _lastIteration = 0;
    public int orgReachabilityBound = 0;
    public String smtReachabilityConstraint = "";
    public List<String> smtIterativeCode = new ArrayList<String>();
    public static List<String> lstPrcNameToOptimize = new ArrayList<String>();//list of process names to be optimized by price
    public static List<String> lstPrcName = new ArrayList<String>();//list of process names
    public static int num_processesToOptimize;//number of given processes to optimize
    public static int num_processes;//number of given processes
    public Map<String, Map<String, String>> orgNameMap = new HashMap<String, Map<String, String>>();//map used for mapping shortcut names to real model names
    public Map<String,String> solverMap = new HashMap<String, String>();//contains the list of data used for processing
}
