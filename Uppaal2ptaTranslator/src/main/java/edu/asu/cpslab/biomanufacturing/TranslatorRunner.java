package edu.asu.cpslab.biomanufacturing;

/**
 * Created by mhekmatnejad on 9/24/18.
 * <p>
 * <p>
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TranslatorRunner {
    public static void main(String[] args) {
        if(args.length == 0 )
        {
            showCommandHelp();
            return;
        }

        if(args[0].equalsIgnoreCase("--upal2pta") || args[0].equalsIgnoreCase("--show-uppaal-trace")) {
            UppaalTranslator.main(args);
        }else if(args[0].equalsIgnoreCase("--smt2pta")){
            if(args[1].startsWith("--iterative") && args[1].endsWith("Optimizer"))
                if(!args[1].equals("--iterative--internalOptimizer") && !args[1].equals("--iterative--bisectionOptimizer")) {
                    showCommandHelp();
                    return;
                }
            SmtModelTranslator.main(args);
        }else{
            showCommandHelp();
        }
    }

    static void showCommandHelp(){
        System.out.println("Please choose --upal2pta, --smt2pta or --show-uppaal-trace as the first argument.");
        System.out.println("parameters:\n--upal2pta [input xml file in uppaal format]");
        System.out.println("parameters:\n--show-uppaal-trace [uppaal generated trace as input txt file]");
        System.out.println("--smt2pta [input smt code file] [-][process name 1] [-][process name 2] ... [-][process name n]");
        System.out.println("--smt2pta --iterative [min step] [max step] [input smt code file] [-][process name 1] [-][process name 2] ... [-][process name n]");
        System.out.println("--smt2pta --iterative--internalOptimizer [min step] [max step] [input smt code file] [-][process name 1] [-][process name 2] ... [-][process name n]");
        System.out.println("--smt2pta --iterative--bisectionOptimizer [min step] [max step] [min cost] [max cost] [delta cost] [input smt code file] [-][process name 1] [-][process name 2] ... [-][process name n]");
    }
}
