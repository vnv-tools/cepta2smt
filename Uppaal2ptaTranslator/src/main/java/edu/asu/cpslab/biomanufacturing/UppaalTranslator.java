package edu.asu.cpslab.biomanufacturing;

import com.uppaal.model.core2.*;

import org.w3c.dom.Element;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

/**
 * Created by mhekmatnejad on 9/17/18.
 * <p>
 * <p>
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class UppaalTranslator {

    static String automatonType = "PricedTimedAutomaton";

    public UppaalTranslator() {
    }

    public UppaalTranslator(String autType) {
        automatonType = autType;
    }

    static Map<String,String> local_var_val_map = new HashMap<String, String>();
    static Map<String,String> local_const_var_val_map = new HashMap<String, String>();
    static List<String> local_clock_lst;
    static Map<String,String> global_var_val_map = new HashMap<String, String>();
    static Map<String,String> global_const_var_val_map = new HashMap<String, String>();
    static List<String> global_clock_lst;


    static Map<String,String> prcVarNameMap = new HashMap<String, String>();//name of the processes and their variable's assigned name
    static Map<String,List<String>> prcNameVarMap = new HashMap<String, List<String>>();//process name, list of variable names
    static Map<String,List<String>> prcParamMap = new HashMap<String, List<String>>();//process variable name, param names
    static Map<String,ArrayList<String>> paramTypeMap = new HashMap<String, ArrayList<String>>();//param type, param names
    static Map<String,ArrayList<String>> prcLocalParamMap = new HashMap<String,ArrayList<String>>();//process variable name, parameters in the processes
    static List<String> prcSystemList = new ArrayList<String>();//list of instantiated processes by variable names
    static List<String> globalClocks = new ArrayList<String>();//list of global clocks

    static void extractProcessNames(String str_dec){
        //remove comments from declarations
        while(str_dec.contains("//")) {
            while(str_dec.indexOf("\n")==0)
                str_dec = str_dec.substring(1,str_dec.length());
            //System.out.println(str_dec+"\n---------------\n");
            String strRest =  str_dec.substring(str_dec.indexOf("//"),str_dec.length());
            str_dec = str_dec.substring(0, str_dec.indexOf("//")) +strRest.substring(strRest.indexOf("\n"), strRest.length());
        }
        String str_decs[] = str_dec.split(";");//parse line by line
        for(int dec_cnt =0; dec_cnt<str_decs.length; dec_cnt++){
            str_decs[dec_cnt] = str_decs[dec_cnt].replaceAll("\n","");
            str_decs[dec_cnt] = str_decs[dec_cnt].replaceAll("const","");

            while (str_decs[dec_cnt].startsWith(" "))
                str_decs[dec_cnt] = str_decs[dec_cnt].replaceFirst(" ","");

            //variable declaration and assignment
            if(str_decs[dec_cnt].contains("=") && str_decs[dec_cnt].split("=")[0].split(" ").length < 2) {
                String dec_tokens[] = str_decs[dec_cnt].split("=");
                //if(dec_tokens.length!=2)
                //    throw new TranslationException("variable assignment must be in the form of var = value;");
                String str_var = dec_tokens[0];
                String str_val = dec_tokens[1].replaceAll(" ","");
                String str_params = "";
                while(str_var.lastIndexOf(" ") == (str_var.length()-1))
                    str_var = str_var.substring(0,str_var.length()-1);
                if(str_var.contains(" "))
                    str_var = str_var.substring(str_var.lastIndexOf(" "), str_var.length());
                str_var = str_var.replaceAll(" ", "");
                if(str_val.contains("(")) {
                    //the order of parameters is important
                    str_params = str_val.substring(str_val.indexOf("(")+1,str_val.indexOf(")")).replaceAll(" ","");
                    str_val = str_val.substring(0, str_val.indexOf("("));
                }
                System.out.println("var: " + str_var + " val: " + str_val);
                prcVarNameMap.put(str_var, str_val);
                if(!prcNameVarMap.keySet().contains(str_val))
                    prcNameVarMap.put(str_val, new ArrayList<String>());
                prcNameVarMap.get(str_val).add(str_var);
                prcParamMap.put(str_var, Arrays.asList(str_params.split(",")));
            }
            else if(str_decs[dec_cnt].toLowerCase().contains("system")){
                String str_sys = str_decs[dec_cnt];
                str_sys = str_sys.substring(str_sys.indexOf("system")+6,str_sys.length());
                String prcs[] = str_sys.split(",");
                for(int i=0; i<prcs.length; i++)
                    prcSystemList.add(prcs[i].replaceAll(" ",""));
            }
            else if(str_decs[dec_cnt].replaceAll(" ","").length()>0){
                //System.out.println(str_decs[dec_cnt]);
                if(str_decs[dec_cnt].contains("="))
                    System.out.println();

                String strType = str_decs[dec_cnt].substring(0,str_decs[dec_cnt].indexOf(" "));
                //if(str_decs[dec_cnt].toLowerCase().contains("chan"));
                String str_chan = str_decs[dec_cnt];
                str_chan = str_chan.substring(str_chan.toLowerCase().indexOf(strType)+strType.length()+1,str_chan.length());
                String chans[];
                if(str_chan.contains("=")) {
                    str_chan = str_chan.substring(0, str_chan.indexOf("="));
                    chans = str_chan.split(" ");
                }
                else
                    chans = str_chan.replaceAll(" ","").split(",");
                if(!paramTypeMap.keySet().contains(strType))
                    paramTypeMap.put(strType,(ArrayList<String>)new ArrayList<String>(Arrays.asList(chans)));
                else{
                    ArrayList<String> lstChan = (ArrayList<String>)paramTypeMap.get(strType);
                    ArrayList<String> addChan = new ArrayList<String>(Arrays.asList(chans));
                    lstChan.addAll(0,addChan);
                    paramTypeMap.put(strType,lstChan);
                }
            }
        }

    }

    static void parsePrcParameters(String strParam, String prcName){
        if(strParam.replaceAll(" ","").length()==0)
            return;
        String params[] = strParam.split(",");
        for(int i=0; i<params.length; i++){
            String param = params[i];
            param= param.replaceAll("const","");
            while (param.startsWith(" "))
                param = param.replaceFirst(" ","");
            param = param.split(" ")[1];
            param = param.replaceAll(" ","").replaceAll("&","");//&
            if(!prcLocalParamMap.keySet().contains(prcName))
                prcLocalParamMap.put(prcName,new ArrayList<String>());
            prcLocalParamMap.get(prcName).add(param);
        }
    }

    static String replaceParamFromLocalDeclaration(String param, String prcName, String prcVarName){
        String globalName = param;
        if(prcLocalParamMap.keySet().contains(prcName) && prcLocalParamMap.get(prcName).contains(param)){
            if(prcParamMap.keySet().contains(prcVarName) && prcSystemList.contains(prcVarName)) {
                int idx = prcLocalParamMap.get(prcName).indexOf(param);
                globalName = prcParamMap.get(prcVarName).get(idx);
            }
        }
        System.out.println("param changed: " + param + " -> " + globalName);
        return globalName;
    }

    static String replaceAllParamFromDeclaration(String formula, String prcName, String prcVarName){
        String newFormula = formula;
        if(!formula.contains("(/"))
            newFormula = formula.replaceAll(" ","");
        if(prcLocalParamMap.keySet().contains(prcName)){
            if(prcParamMap.keySet().contains(prcVarName) && prcSystemList.contains(prcVarName)) {
                for(int idx = 0; idx < prcLocalParamMap.get(prcName).size(); idx++) {
                    String key= "("+prcLocalParamMap.get(prcName).get(idx)+")";
                    if(newFormula.contains(key)) {
                        key= "\\("+prcLocalParamMap.get(prcName).get(idx)+"\\)";
                        newFormula = newFormula.replaceAll(key, "(" + prcParamMap.get(prcVarName).get(idx) + ")");
                    }
                    else {
                        key = "\\(\\-" + prcLocalParamMap.get(prcName).get(idx) + "\\)";
                        newFormula = newFormula.replaceAll(key, "(-" + prcParamMap.get(prcVarName).get(idx) + ")");
                    }
                }
            }
        }
        System.out.println("param changed: " + formula + " -> " + newFormula);
        return newFormula;
    }

    //add local clocks into local_clock_lst and constants into local_var_val_map
    static void parseLocalDeclarations(Node template) {

        //clear the local containers
        local_const_var_val_map.clear();
        local_var_val_map.clear();

            System.out.println("Name: " + template.getPropertyValue("name"));


            String str_dec = template.getPropertyValue("declaration").toString();
            //remove comments
            while(str_dec.contains("//")) {
                while(str_dec.indexOf("\n")==0)
                    str_dec = str_dec.substring(1,str_dec.length());
                String strRest =  str_dec.substring(str_dec.indexOf("//"),str_dec.length());
                if(strRest.indexOf("\n")>0)
                    str_dec = str_dec.substring(0, str_dec.indexOf("//")) +strRest.substring(strRest.indexOf("\n"), strRest.length());
                else
                    str_dec = str_dec.substring(0, str_dec.indexOf("//"));
            }
            //read declarations line by line
            String str_decs[] = str_dec.split(";");
            for(int dec_cnt =0; dec_cnt<str_decs.length; dec_cnt++){
                str_decs[dec_cnt] = str_decs[dec_cnt].replaceAll("\n","");
                //variable declaration and assignment
                if(str_decs[dec_cnt].contains("=")) {
                    boolean isConstant = false;
                    String dec_tokens[] = str_decs[dec_cnt].split("=");
                    //if(dec_tokens.length!=2)
                    //    throw new TranslationException("variable assignment must be in the form of var = value;");
                    String str_var = dec_tokens[0];
                    String str_val = dec_tokens[1].replaceAll(" ","");
                    while(str_var.lastIndexOf(" ") == (str_var.length()-1))
                        str_var = str_var.substring(0, str_var.length()-1);
                    while(str_var.startsWith(" "))
                        str_var = str_var.replaceFirst(" ","");
                    if(str_var.startsWith("const "))
                        isConstant = true;
                    if(str_var.contains(" "))
                        str_var = str_var.substring(str_var.lastIndexOf(" "), str_var.length());
                    str_var = str_var.replaceAll(" ", "");
                    //System.out.println("var: " + str_var + " val: " + str_val);
                    if(str_val.contains("/")) {
                        String lr[] = str_val.split("/");
                        if(!lr[0].contains("."))
                            lr[0] += ".0";
                        if(!lr[1].contains("."))
                            lr[1] += ".0";
                        str_val = "(/ "+lr[0]+" " +lr[1]+")";
                    }
                    if(isConstant)
                        local_const_var_val_map.put(str_var,str_val);
                    else
                        local_var_val_map.put(str_var, str_val);
                }
                else if(str_decs[dec_cnt].toLowerCase().contains("clock")){
                    String str_clk = str_decs[dec_cnt];
                    str_clk = str_clk.substring(str_clk.toLowerCase().indexOf("clock")+5,str_clk.length());
                    str_clk = str_clk.replaceAll(" ","");
                    local_clock_lst.add(str_clk);
                }
                else if(str_decs[dec_cnt].toLowerCase().contains("int")){
                    if(str_decs[dec_cnt].toLowerCase().contains("const ")){
                        System.err.println("Constants must have values.");
                        System.exit(0);
                    }
                    String str_invariant = str_decs[dec_cnt];
                    str_invariant = str_invariant.substring(str_invariant.toLowerCase().indexOf("int")+3,str_invariant.length());
                    str_invariant = str_invariant.replaceAll(" ","");
                    local_var_val_map.put(str_invariant, "0");
                }
            }//end for
    }

    //add global clocks into global_clock_lst and constants into global_var_val_map
    static void parseGlobalDeclarations(String str_dec) {


        System.out.println("Global declarations: ");

        //remove comments
        while(str_dec.contains("//")) {
            while(str_dec.indexOf("\n")==0)
                str_dec = str_dec.substring(1,str_dec.length());
            String strRest =  str_dec.substring(str_dec.indexOf("//"),str_dec.length());
            if(strRest.indexOf("\n")>0)
                str_dec = str_dec.substring(0, str_dec.indexOf("//")) +strRest.substring(strRest.indexOf("\n"), strRest.length());
            else
                str_dec = str_dec.substring(0, str_dec.indexOf("//"));
        }
        //read declarations line by line
        String str_decs[] = str_dec.split(";");
        for(int dec_cnt =0; dec_cnt<str_decs.length; dec_cnt++){
            str_decs[dec_cnt] = str_decs[dec_cnt].replaceAll("\n","");
            //variable declaration and assignment
            if(str_decs[dec_cnt].contains("=")) {
                boolean isConstant = false;
                String dec_tokens[] = str_decs[dec_cnt].split("=");
                //if(dec_tokens.length!=2)
                //    throw new TranslationException("variable assignment must be in the form of var = value;");
                String str_var = dec_tokens[0];
                String str_val = dec_tokens[1].replaceAll(" ","");
                while(str_var.lastIndexOf(" ") == (str_var.length()-1))
                    str_var = str_var.substring(0,str_var.length()-1);
                if(str_var.startsWith("const "))
                    isConstant = true;
                if(str_var.contains(" "))
                    str_var = str_var.substring(str_var.lastIndexOf(" "), str_var.length());
                str_var = str_var.replaceAll(" ", "");
                //System.out.println("var: " + str_var + " val: " + str_val);
                if(str_val.contains("/")) {
                    String lr[] = str_val.split("/");
                    if(!lr[0].contains("."))
                        lr[0] += ".0";
                    if(!lr[1].contains("."))
                        lr[1] += ".0";
                    str_val = "(/ "+lr[0]+" " +lr[1]+")";
                }
                if(isConstant)
                    global_const_var_val_map.put(str_var,str_val);
                global_var_val_map.put(str_var, str_val);//this contains both constants and variables
            }
            else if(str_decs[dec_cnt].toLowerCase().contains("clock")){
                String str_clk = str_decs[dec_cnt];
                str_clk = str_clk.substring(str_clk.toLowerCase().indexOf("clock")+5,str_clk.length());
                str_clk = str_clk.replaceAll(" ","");
                global_clock_lst.add(str_clk);
            }
        }
    }

    //main entry to the translation
    static void translateFile(String strXMLfileName, boolean isFile) {
        //Uppaal xml reader
        System.out.println("Translation started...");
        UppaalTranslator upt = new UppaalTranslator();

        try {
            URL url = null;
            if (!isFile)
                url = ((URI) upt.getClass().getClassLoader().getResource(strXMLfileName).toURI()).toURL();
            else {
                url = new File(strXMLfileName).toURI().toURL();
            }
            Document rd_doc = new PrototypeDocument().load(url);
            //writing to XML file
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            // root elements
            org.w3c.dom.Document wt_doc = docBuilder.newDocument();

            Element aut_elm = null;
            Element taut_elm = null;

            String strSysProp = (((Document) rd_doc).getFirst().getParent().getPropertyValue("system")).toString();
            //process the system declaration to find out how many processes exists by name
            extractProcessNames(strSysProp);

            global_clock_lst = new ArrayList<String>();
            String strGlobalDecProp = (((Document) rd_doc).getFirst().getParent().getPropertyValue("declaration")).toString();
            //extract global declarations
            parseGlobalDeclarations(strGlobalDecProp);

            Node template = rd_doc.getFirst();



            //-----MAIN LOOP----//

            while (template != null) {

                if (template instanceof Template) {
                    String prc_name = template.getPropertyValue("name").toString();

                    //List<String> prc_nameInSystemDec = new ArrayList<String>();
                    //id this is a new process

                    local_clock_lst = new ArrayList<String>();
                    parseLocalDeclarations(template);//read clock and const variables

                    //keeps the list of needed processes (more than one process in the system per prce name)
                    //for (String pn : prcVarNameMap.keySet())
                    //    if (prcVarNameMap.get(pn).equals(prc_name)) {
                    //        prc_nameInSystemDec.add(pn);
                    //    }


                    //parse process parameters
                    String str_param = template.getPropertyValue("parameter").toString();
                    parsePrcParameters(str_param, prc_name);//load process's parameters

                    if(prcNameVarMap.get(prc_name) != null)
                    for (int samePrcCount = 0; samePrcCount < prcNameVarMap.get(prc_name).size(); samePrcCount++) {

                        String prc_var_name = prcNameVarMap.get(prc_name).get(samePrcCount);


                        docFactory = DocumentBuilderFactory.newInstance();
                        docBuilder = docFactory.newDocumentBuilder();
                        // root elements
                        wt_doc = docBuilder.newDocument();
                        aut_elm = wt_doc.createElement("Automata");
                        wt_doc.appendChild(aut_elm);
                        taut_elm = wt_doc.createElement(automatonType);
                        aut_elm.appendChild(taut_elm);
                        Element aut_name_elm = wt_doc.createElement("Name");
                        taut_elm.appendChild(aut_name_elm);
                        Element aut_text_elm = wt_doc.createElement("Text");
                        aut_name_elm.appendChild(aut_text_elm);
                        aut_text_elm.appendChild(wt_doc.createTextNode(prc_var_name));
                        //add local clocks
                        for (int i = 0; i < local_clock_lst.size(); i++) {
                            Element clock_elm = wt_doc.createElement("Clock");
                            taut_elm.appendChild(clock_elm);
                            Element clock_name_elm = wt_doc.createElement("Text");
                            clock_elm.appendChild(clock_name_elm);
                            clock_name_elm.appendChild(wt_doc.createTextNode(local_clock_lst.get(i)));
                        }
                        //add global clocks
                        for (int i = 0; i < global_clock_lst.size(); i++) {
                            Element clock_elm = wt_doc.createElement("Clock");
                            taut_elm.appendChild(clock_elm);
                            Element clock_name_elm = wt_doc.createElement("Text");
                            clock_elm.appendChild(clock_name_elm);
                            clock_name_elm.appendChild(wt_doc.createTextNode(global_clock_lst.get(i)));
                            Element clock_global_elm = wt_doc.createElement("Global");
                            clock_elm.appendChild(clock_global_elm);
                        }
                        //add global declarations
                        Element decls_elm = null;
                        if(global_var_val_map.size() > 0 || local_var_val_map.size() > 0) {
                            decls_elm = wt_doc.createElement("Declarations");
                            taut_elm.appendChild(decls_elm);
                        }
                        if(global_var_val_map.size() > 0) {
                            for (Map.Entry<String,String> keyval : global_var_val_map.entrySet())
                            if(!global_const_var_val_map.containsKey(keyval.getKey()))//ignore constant ones
                            {
                                Element decl_elm = wt_doc.createElement("Declare");
                                decls_elm.appendChild(decl_elm);
                                Element decl_name_elm = wt_doc.createElement("Text");
                                decl_elm.appendChild(decl_name_elm);
                                decl_name_elm.appendChild(wt_doc.createTextNode(keyval.getKey()));
                                Element int_val_elm = wt_doc.createElement("Default");
                                int_val_elm.appendChild(wt_doc.createTextNode(keyval.getValue()));
                                decl_elm.appendChild(int_val_elm);
                                Element declare_global_elm = wt_doc.createElement("Global");
                                decl_elm.appendChild(declare_global_elm);
                            }
                        }
                        //add local declarations
                        if(local_var_val_map.size() > 0) {
                            for (Map.Entry<String,String> keyval : local_var_val_map.entrySet())
                                if(!local_const_var_val_map.containsKey(keyval.getKey()))//ignore constant ones
                                {
                                    Element decl_elm = wt_doc.createElement("Declare");
                                    decls_elm.appendChild(decl_elm);
                                    Element decl_name_elm = wt_doc.createElement("Text");
                                    decl_elm.appendChild(decl_name_elm);
                                    decl_name_elm.appendChild(wt_doc.createTextNode(keyval.getKey()));
                                    Element int_val_elm = wt_doc.createElement("Default");
                                    int_val_elm.appendChild(wt_doc.createTextNode(keyval.getValue()));
                                    decl_elm.appendChild(int_val_elm);
                                }
                        }
                        Node node = template.getFirst();

                        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
                        readXmlDoc(node, prc_name, prc_var_name, taut_elm, wt_doc);
                        writeXmlDoc(wt_doc, prc_name, prc_var_name);
                        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//


                    }//repetitive process for different variables declared in system declaration
                }
                template = template.getNext();
            }//loop for all nodes of the Uppaal xml document

        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static void readXmlDoc(Node node, String prc_name, String prc_val_name, Element taut_elm, org.w3c.dom.Document wt_doc) {

        Boolean isInit = false;
        while (node != null) {
            isInit = false;
            //this is a new location
            if (node instanceof Location) {
                Location loc = (Location) node;
                System.out.println("State Name: " + loc.getProperty("name").getValue().toString());
                System.out.println("Cost function: " + loc.getPropertyValue("comments"));//we encode cost function as comment

                if (loc.getPropertyValue("init") != null && Boolean.parseBoolean(loc.getPropertyValue("init").toString())) {
                    System.out.println("Is initial state: " + loc.getPropertyValue("init"));
                    isInit = true;
                }
                Element loc_elm = wt_doc.createElement("Location");
                taut_elm.appendChild(loc_elm);
                Element name_elm = wt_doc.createElement("Name");
                loc_elm.appendChild(name_elm);
                name_elm.appendChild(wt_doc.createTextNode(loc.getProperty("name").getValue().toString()));
                if (isInit)
                    loc_elm.appendChild(wt_doc.createElement("Initial"));
                if (!loc.getPropertyValue("comments").toString().equals("")) {//this is price function
                    String prc = loc.getPropertyValue("comments").toString();
                    //local const variable replacement with values
                    SortedSet<String> keysSet = new TreeSet<String>(local_const_var_val_map.keySet());
                    List<String> keys = new ArrayList<String>(keysSet);
                    Collections.sort(keys, Collections.reverseOrder());
                    for (int i = 0; i < keys.size(); i++) {
                        prc = prc.replaceAll(keys.get(i), local_const_var_val_map.get(keys.get(i)));
                    }
                    //global const variable replacement with values
                    keysSet = new TreeSet<String>(global_const_var_val_map.keySet());
                    keys = new ArrayList<String>(keysSet);
                    Collections.sort(keys, Collections.reverseOrder());
                    for (int i = 0; i < keys.size(); i++) {
                        prc = prc.replaceAll(keys.get(i), global_const_var_val_map.get(keys.get(i)));
                    }
                    prc = replaceAllParamFromDeclaration(prc,prc_name,prc_val_name);
                    Element prc_elm = wt_doc.createElement("Price");
                    loc_elm.appendChild(prc_elm);
                    Element txt_elm = wt_doc.createElement("Text");
                    prc_elm.appendChild(txt_elm);
                    txt_elm.appendChild(wt_doc.createTextNode(prc));
                }
                if(!loc.getPropertyValue("invariant").toString().equals("")){//invariant in locations
                    String asg = loc.getPropertyValue("invariant").toString();
                    String asgs[] = asg.split("&&");
                    for (int asg_num = 0; asg_num < asgs.length; asg_num++) {
                        asg = asgs[asg_num];
                        asg = asg.replaceAll("\n", "");
                        asg = asg.replaceAll("\t", "");


                        Element grd_elm = wt_doc.createElement("Invariant");
                        loc_elm.appendChild(grd_elm);
                        Element txt_elm = wt_doc.createElement("Text");
                        grd_elm.appendChild(txt_elm);

                        //we have to sort the variable names st the longer names comes first
                        SortedSet<String> keysSet = new TreeSet<String>(local_const_var_val_map.keySet());
                        List<String> keys = new ArrayList<String>(keysSet);
                        Collections.sort(keys, Collections.reverseOrder());
                        for (int i = 0; i < keys.size(); i++) {
                            if(!asg.replaceAll(" ","").startsWith(keys.get(i)))
                                asg = asg.replaceAll(keys.get(i), local_const_var_val_map.get(keys.get(i)));
                        }
                        //global const variable replacement with values
                        keysSet = new TreeSet<String>(global_const_var_val_map.keySet());
                        keys = new ArrayList<String>(keysSet);
                        Collections.sort(keys, Collections.reverseOrder());
                        for (int i = 0; i < keys.size(); i++) {
                            if(!asg.replaceAll(" ","").startsWith(keys.get(i)))
                                asg = asg.replaceAll(keys.get(i), global_const_var_val_map.get(keys.get(i)));
                        }

                        String sign = ">";
                        String new_sign = "<";
                        if (asg.contains(sign) && asg.contains(sign + "=")) {
                            sign += "=";
                            new_sign += "=";
                        }
                        if (asg.contains(sign)) {
                            String tokens[] = asg.split(sign);
                            asg = "-" + tokens[0].replaceAll(" ", "").replaceAll("\n", "") + " " +
                                    new_sign + " " + "-" + tokens[1].replaceAll(" ", "").replaceAll("\n", "");
                        }
                        //replace const variables with values
                        if(asg.contains("=="))
                            sign = "==";
                        else if(asg.contains("<="))
                            sign = "<=";
                        else
                            sign = "<";
                        String tokens[] = asg.split(sign);
                        tokens[0] = tokens[0].replaceAll(" ","");
                        tokens[1] = tokens[1].replaceAll(" ","");
                        String negation = "";
                        if(tokens[0].startsWith("-"))
                            negation = "-";
                        tokens[0] = negation + replaceParamFromLocalDeclaration(tokens[0].replaceAll("-",""), prc_name, prc_val_name);
                        negation = "";
                        if(tokens[1].startsWith("-"))
                            negation = "-";
                        tokens[1] = negation + replaceParamFromLocalDeclaration(tokens[1].replaceAll("-",""), prc_name, prc_val_name);
                        asg = tokens[0] + sign + tokens[1];

                        txt_elm.appendChild(wt_doc.createTextNode(asg));
                    }

                }

            }//this is a new edge
            else if (node instanceof Edge) {

                Edge edge = (Edge) node;
                System.out.println("\nEdge: " + ((Location) edge.getSource()).getProperty("name").getValue() + " -> " + ((Location) edge.getTarget()).getProperty("name").getValue());
                System.out.println(edge.getPropertyValue("guard"));
                System.out.println(edge.getPropertyValue("synchronisation"));
                System.out.println(edge.getPropertyValue("assignment"));
                System.out.println(edge.getPropertyValue("comments"));//we encode cost function as comment

                Element tr_elm = wt_doc.createElement("Transition");
                taut_elm.appendChild(tr_elm);
                Element src_elm = wt_doc.createElement("Source");
                tr_elm.appendChild(src_elm);
                src_elm.appendChild(wt_doc.createTextNode(((Location) edge.getSource()).getProperty("name").getValue().toString()));
                Element dst_elm = wt_doc.createElement("Destination");
                tr_elm.appendChild(dst_elm);
                dst_elm.appendChild(wt_doc.createTextNode(((Location) edge.getTarget()).getProperty("name").getValue().toString()));

                if (!edge.getPropertyValue("synchronisation").toString().equals("")) {
                    String alp = edge.getPropertyValue("synchronisation").toString();
                    String alps[] = alp.split(",");
                    for (int alp_num = 0; alp_num < alps.length; alp_num++) {
                        alp = alps[alp_num];
                        alp = alp.replaceAll(" ","").replaceAll("\n", "");
                        String varName = alp.substring(0, alp.length()-1);
                        alp = replaceParamFromLocalDeclaration(varName, prc_name, prc_val_name) + alp.charAt(alp.length()-1);
                        Element alp_elm = wt_doc.createElement("Alphabet");
                        tr_elm.appendChild(alp_elm);
                        Element alp_text_elm = wt_doc.createElement("Text");
                        alp_elm.appendChild(alp_text_elm);
                        alp_text_elm.appendChild(wt_doc.createTextNode(alp));
                    }
                }

                if (!edge.getPropertyValue("assignment").toString().equals("")) {
                    String clk = edge.getPropertyValue("assignment").toString();
                    String clks[] = clk.split(",");
                    for (int clk_num = 0; clk_num < clks.length; clk_num++) {
                        clk = clks[clk_num];
                        String sign = "++";
                        if(!clk.contains(sign)) {
                            sign = "--";
                            if(!clk.contains(sign)){
                                sign = "-=";
                                if(!clk.contains(sign)){
                                    sign = "+=";
                                    if(!clk.contains(sign)){
                                        sign = "=";
                                    }
                                }
                            }
                        }

                        clk = clk.substring(0, clk.indexOf(sign));
                        clk = clk.replaceAll(" ", "").replaceAll("\n", "");
                        if(local_clock_lst.contains(clk)) {//this is a clock to be reset
                            Element rst_elm = wt_doc.createElement("ResetClock");
                            tr_elm.appendChild(rst_elm);
                            Element txt_elm = wt_doc.createElement("Text");
                            rst_elm.appendChild(txt_elm);
                            txt_elm.appendChild(wt_doc.createTextNode(clk));
                        }
                        else if(local_var_val_map.keySet().contains(clk) || global_var_val_map.keySet().contains(clk)){//this is a variable to be updated
                            //todo: in future mathematical expressions should be allowed on the RHS of update equations
                            String update = clks[clk_num];
                            update = update.replaceAll("\n", "");
                            String var="", val="";
                            boolean validUpdate = false;
                            if(update.contains("+=") || update.contains("-=")) {
                                var = update.substring(0, update.indexOf("=")-1);
                                var = var.replaceAll(" ", "");
                                String opt = update.substring(update.indexOf("=")-1,update.indexOf("=")+1);
                                val = update.substring(update.indexOf("=")+1,update.length()).replaceAll(" ","");
                                boolean isConst = true;
                                try {
                                    Integer.parseInt(val);
                                } catch (NumberFormatException e) {
                                    if(local_const_var_val_map.keySet().contains(clk))
                                        val = local_const_var_val_map.get(val);
                                    else if(global_const_var_val_map.keySet().contains(clk))
                                        val = global_const_var_val_map.get(val);
                                    else
                                        isConst = false;
                                }
                                if(update.contains("-="))
                                    val = "-"+val;
                                validUpdate = true;
                            }
                            else if(update.contains("++") || update.contains("--")){
                                sign = "++";
                                if(!update.contains(sign))
                                    sign = "--";
                                var = update.substring(0, update.indexOf(sign));
                                var = var.replaceAll(" ", "");
                                val = "1";
                                if(sign.equals("--"))
                                    val = "-1";
                                validUpdate = true;
                            }
                            if(validUpdate) {
                                Element rst_elm = wt_doc.createElement("Update");
                                tr_elm.appendChild(rst_elm);
                                Element txt_elm = wt_doc.createElement("Text");
                                rst_elm.appendChild(txt_elm);
                                txt_elm.appendChild(wt_doc.createTextNode(var + "+=" + val));
                            }
                            else if( update.contains("=") ) {//this is a reset to const/variable value
                                sign = "=";
                                var = update.substring(0, update.indexOf(sign));
                                var = var.replaceAll(" ", "");
                                val = update.substring(update.indexOf(sign)+1,update.length()).replaceAll(" ","");
                                boolean isConst = true;
                                try {
                                    Integer.parseInt(val);
                                } catch (NumberFormatException e) {
                                    if(local_const_var_val_map.keySet().contains(clk))
                                        val = local_const_var_val_map.get(val);
                                    else if(global_const_var_val_map.keySet().contains(clk))
                                        val = global_const_var_val_map.get(val);
                                    else
                                        isConst = false;
                                }

                                Element rst_elm = wt_doc.createElement("Update");
                                tr_elm.appendChild(rst_elm);
                                Element txt_elm = wt_doc.createElement("Text");
                                rst_elm.appendChild(txt_elm);
                                txt_elm.appendChild(wt_doc.createTextNode(var + "=" + val));
                            }
                        }
                    }
                }

                if (!edge.getPropertyValue("guard").toString().equals("")) {
                    String asg = edge.getPropertyValue("guard").toString();
                    String asgs[] = asg.split("&&");
                    for (int asg_num = 0; asg_num < asgs.length; asg_num++) {
                        asg = asgs[asg_num];
                        asg = asg.replaceAll("\n", "");
                        asg = asg.replaceAll("\t", "");


                        Element grd_elm = wt_doc.createElement("Guard");
                        tr_elm.appendChild(grd_elm);
                        Element txt_elm = wt_doc.createElement("Text");
                        grd_elm.appendChild(txt_elm);

                        //we have to sort the variable names st the longer names comes first
                        SortedSet<String> keysSet = new TreeSet<String>(local_const_var_val_map.keySet());
                        List<String> keys = new ArrayList<String>(keysSet);
                        Collections.sort(keys, Collections.reverseOrder());
                        for (int i = 0; i < keys.size(); i++) {
                            if(!asg.replaceAll(" ","").startsWith(keys.get(i)))
                                asg = asg.replaceAll(keys.get(i), local_const_var_val_map.get(keys.get(i)));
                        }
                        //global const variable replacement with values
                        keysSet = new TreeSet<String>(global_const_var_val_map.keySet());
                        keys = new ArrayList<String>(keysSet);
                        Collections.sort(keys, Collections.reverseOrder());
                        for (int i = 0; i < keys.size(); i++) {
                            if(!asg.replaceAll(" ","").startsWith(keys.get(i)))
                                asg = asg.replaceAll(keys.get(i), global_var_val_map.get(keys.get(i)));
                        }

                        String sign = ">";
                        String new_sign = "<";
                        if (asg.contains(sign) && asg.contains(sign + "=")) {
                            sign += "=";
                            new_sign += "=";
                        }
                        if (asg.contains(sign)) {
                            String tokens[] = asg.split(sign);
                            asg = "-" + tokens[0].replaceAll(" ", "").replaceAll("\n", "") + " " +
                                    new_sign + " " + "-" + tokens[1].replaceAll(" ", "").replaceAll("\n", "");
                        }
                        //replace const variables with values
                        if(asg.contains("=="))
                            sign = "==";
                        else if(asg.contains("<="))
                            sign = "<=";
                        else
                            sign = "<";
                        String tokens[] = asg.split(sign);
                        tokens[0] = tokens[0].replaceAll(" ","");
                        tokens[1] = tokens[1].replaceAll(" ","");
                        String negation = "";
                        if(tokens[0].startsWith("-"))
                            negation = "-";
                        tokens[0] = negation + replaceParamFromLocalDeclaration(tokens[0].replaceAll("-",""), prc_name, prc_val_name);
                        negation = "";
                        if(tokens[1].startsWith("-"))
                            negation = "-";
                        tokens[1] = negation + replaceParamFromLocalDeclaration(tokens[1].replaceAll("-",""), prc_name, prc_val_name);
                        asg = tokens[0] + sign + tokens[1];

                        txt_elm.appendChild(wt_doc.createTextNode(asg));
                    }
                }

                if (!edge.getPropertyValue("comments").toString().equals("")) {//this is price function
                    String prc = edge.getPropertyValue("comments").toString();
                    //we have to sort the variable names st the longer names comes first
                    SortedSet<String> keysSet = new TreeSet<String>(local_const_var_val_map.keySet());
                    List<String> keys = new ArrayList<String>(keysSet);
                    Collections.sort(keys, Collections.reverseOrder());
                    for (int i = 0; i < keys.size(); i++) {
                        prc = prc.replaceAll(keys.get(i), local_const_var_val_map.get(keys.get(i)));
                    }
                    //global const variable replacement with values
                    keysSet = new TreeSet<String>(global_const_var_val_map.keySet());
                    keys = new ArrayList<String>(keysSet);
                    Collections.sort(keys, Collections.reverseOrder());
                    for (int i = 0; i < keys.size(); i++) {
                        prc = prc.replaceAll(keys.get(i), global_const_var_val_map.get(keys.get(i)));
                    }
                    prc = replaceAllParamFromDeclaration(prc,prc_name,prc_val_name);

                    Element prc_elm = wt_doc.createElement("Price");
                    tr_elm.appendChild(prc_elm);
                    Element txt_elm = wt_doc.createElement("Text");
                    prc_elm.appendChild(txt_elm);
                    txt_elm.appendChild(wt_doc.createTextNode(prc));
                }

            }
            node = node.getNext();
        }

    }

    static void writeXmlDoc(org.w3c.dom.Document wt_doc, String prc_name, String prc_val_name){

        try {
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");


            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            //initialize StreamResult with File object to save to file


            //for (int i = 0; i < prc_nameInSystemDec.size(); i++) {
            //    prc_name = prc_nameInSystemDec.get(i);

            wt_doc.getElementsByTagName("Name").item(0).getFirstChild().getFirstChild().setNodeValue(prc_val_name);

                DOMSource source = new DOMSource(wt_doc);
                StreamResult result = new StreamResult(new File(prc_val_name + ".xml"));
                //String xmlString = result.getWriter().toString();
                //System.out.println(xmlString);
                // Output to console for testing
                // StreamResult result = new StreamResult(System.out);

                transformer.transform(source, result);
                System.out.println("======================================================================");
                System.out.println("Process <" + prc_val_name + "> generated successfully!");
                System.out.println("======================================================================");
            //}

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public class TranslationException extends Exception{

    }

    public static void main(String[] args) {
        //String strFileName = "uppaal/bioman-linear-compact-3-CORA.xml";
        //boolean isFile = false;
        String strFileName = "test-nonlinear-ND.xml";
        boolean isFile = true;

        if(args.length>1) {
            if (args[0].equals("--show-uppaal-trace")) {
                strFileName = args[1];
                parseUppaalTrace(strFileName);
            }
            else {
                strFileName = args[1];
                File file = new File(strFileName);
                if (!file.isAbsolute()) {
                    strFileName = "" + System.getProperty("user.dir") + "/" + strFileName;
                    System.out.println(strFileName);
                    isFile = true;
                    file = new File(strFileName);
                    if (!file.exists()) {
                        System.out.println("Error! the input file name as the argument is not accessible.");
                        return;
                    }
                }
            }
        }
        translateFile(strFileName, isFile);

    }

    static void parseUppaalTrace(String strFileName){

        try {
            //BufferedReader br = new BufferedReader(new FileReader(new File("src/main/resources/uppaal/Linear.txt")));
            BufferedReader br = new BufferedReader(new FileReader(new File(strFileName)));
            String line = "";//br.readLine();
            int cntState = 0;
            int cntTrans = 0;
            int cntDelay = 0;
            boolean isState = false;
            boolean isDelay = false;
            boolean isTrans = false;
            String text = "";
            String strDelay = "";
            String strState = "";
            String strTrans = "";
            int counter = 0;
            boolean endOfFile = false;
            System.out.println("STEP <" + counter + ">");

            while (line!=null)
            {
                line = br.readLine();
                if(line!=null)
                    line = line.replaceAll("\n","");
                else
                    endOfFile = true;

                if(endOfFile || line.startsWith("Delay:")) {
                    cntDelay++;
                    if(isState)
                        strState = text;
                    else if(isTrans)
                        strTrans = text;
                    isDelay = true;
                    isState = false;
                    isTrans = false;
                    if(!endOfFile)
                        text = line;
                    else
                        text = null;
                }
                else if(line.startsWith("State:")) {
                    cntState++;
                    if(isDelay)
                        strDelay = text;
                    else if(isTrans)
                        strTrans = text;
                    isState = true;
                    isDelay = false;
                    isTrans = false;
                    text = null;
                }
                else if(line.startsWith("Transitions:")) {
                    cntTrans++;
                    if(isDelay)
                        strDelay = text;
                    else if(isState)
                        strState = text;
                    isTrans = true;
                    isDelay = false;
                    isState = false;
                    text = null;
                }
                else
                    text += line;

                if(text==null && cntState>=1) {
                    text = "";
                }
                if(strDelay.length()>0 || strState.length()>0 || strTrans.length()>0)
                {
                    if(strDelay.length()>0 || strTrans.length()>0 ) {
                        counter++;
                        System.out.println("STEP <" + counter + ">");
                    }
                    if(strState.length()>0)
                    {
                        String strVars = strState.substring(strState.indexOf(")")+1,strState.length());
                        strState = strState.substring(strState.indexOf("(")+1,strState.indexOf(")"));
                        System.out.println("states: "+strState);
                        System.out.println("variables: "+strVars+"\n=====================\n");
                        strState = "";
                        isState = false;
                    }
                    else if(strDelay.length()>0)
                    {
                        System.out.println("delay: "+strDelay+"\n---------------------");
                        strDelay = "";
                        isDelay = false;
                    }
                    else if(strTrans.length()>0)
                    {
                        String strVars = "";
                        String strMove = "";
                        while(strTrans.contains("->")) {
                            strVars = strTrans.substring(strTrans.indexOf("{") + 1, strTrans.indexOf("}"));
                            strMove = strTrans.substring(0, strTrans.indexOf("{"));
                            strTrans = strTrans.substring(strTrans.indexOf("}")+1,strTrans.length());
                            System.out.println("transitions: "+strMove);
                            System.out.println("variables: "+strVars);
                        }
                        System.out.println("---------------------");
                        strTrans = "";
                        isTrans = false;
                    }
                }
                //if(cntState>0)
                //    System.out.println(line);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    }
