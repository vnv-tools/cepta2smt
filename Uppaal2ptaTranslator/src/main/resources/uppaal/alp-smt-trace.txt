.\verifyta.exe : 
At line:1 char:1
+ .\verifyta.exe -t3  .\alp-smt.xml .\alp-new.q 1>info.txt 2>alp-smt.tx ...
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:String) [], RemoteException
    + FullyQualifiedErrorId : NativeCommandError
 
State:
( runway0.Init runway1.Init KL101.approaching
 KL108.approaching 
KL115.approaching
 KL122.
approaching KL129.
approaching KL136
.approaching 
KL143.
approaching KL150
.approaching KL157
.approaching 
KL164.
approaching )

time=
0 runway0.c0
=0 
runway0.c1=0
 runway1.c0=
0 runway1.c1=
0 rate=
0 cost=0


Delay: 
98


State:
( runway0
.Init 
runway1.Init
 KL101.approaching KL108.approaching KL115.approaching KL122
.approaching
 KL129.approaching
 KL136
.approaching 
KL143.approaching
 KL150.
approaching KL157
.approaching 
KL164.approaching
 )
time
=98 
runway0.c0=98
 runway0.c1=
98 runway1.c0
=98 
runway1.c1=98
 rate=
0 cost=
0

Transitions:

  KL115.
approaching->KL115.
on_time { 
time >= E, 
land1!, 1
 }

  runway0.
Init->runway0
.loop { 
1, land1?
, c1 := 0 }


State:
( 
runway0.loop
 runway1.
Init KL101
.approaching 
KL108.approaching
 KL115.
on_time KL122
.approaching KL129
.approaching 
KL136.approaching
 KL143.
approaching KL150
.approaching 
KL157.approaching
 KL164.
approaching )

time=98 runway0.c0
=98 runway0.c1
=0 
runway1.c0=98
 runway1.c1=
98 rate=
30 cost=0


Transitions:
  
KL115.on_time
->KL115.
done { time == T, 
tau, 1
 }


State:
( runway0
.loop 
runway1.Init
 KL101.
approaching KL108
.approaching 
KL115.done
 KL122.approaching
 KL129.
approaching KL136
.approaching 
KL143.approaching
 KL150.
approaching KL157
.approaching 
KL164.approaching
 )
time
=98 runway0.c0
=98 runway0.c1=0 runway1.c0
=98 
runway1.c1=
98
 rate=0 cost=0

Delay: 
25

State:
( 
runway0.loop runway1.Init KL101.approaching KL108
.approaching KL115.
done KL122.approaching 
KL129.approaching 
KL136.approaching 
KL143.approaching 
KL150.approaching 
KL157.approaching 
KL164.approaching 
)
time=123
 runway0.c0=123
 runway0.c1=25
 runway1.c0=123 
runway1.c1=123 
rate=0 cost=0


Transitions:
  KL129.approaching->KL129.on_time { time >= E, land1!, 1 }
  runway0
.loop->runway0
.loop { c0 >= wait01 && c1 >= wait11, 
land1?, c1 := 0 }

State:
( runway0.loop runway1.Init KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.on_time 
KL136.approaching KL143.approaching KL150.approaching KL157.approaching KL164.approaching )
time=123 runway0.c0=123 runway0.c1=0 runway1.c0=123 runway1.c1=123 rate=30 cost=0

Transitions:
  KL129.on_time->KL129.done { time == T, tau, 1 }

State:
( runway0.loop runway1.Init KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done 
KL136.approaching KL143.approaching KL150.approaching KL157.approaching KL164.approaching )
time=123 runway0.c0=123 runway0.c1=0 runway1.c0=123 runway1.c1=123 rate=0 cost=0

Delay: 9

State:
( runway0.loop runway1.Init KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done 
KL136.approaching KL143.approaching KL150.approaching KL157.approaching KL164.approaching )
time=132 runway0.c0=132 runway0.c1=9 runway1.c0=132 runway1.c1=132 rate=0 cost=0

Transitions:
  KL136.approaching->KL136.on_time { time >= E, land1!, 1 }
  runway1.Init->runway1.loop { 1, land1?, c1 := 0 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.on_time 
KL143.approaching KL150.approaching KL157.approaching KL164.approaching )
time=132 runway0.c0=132 runway0.c1=9 runway1.c0=132 runway1.c1=0 rate=30 cost=0

Delay: 3

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.on_time 
KL143.approaching KL150.approaching KL157.approaching KL164.approaching )
time=135 runway0.c0=135 runway0.c1=12 runway1.c0=135 runway1.c1=3 rate=30 cost=90

Transitions:
  KL136.on_time->KL136.done { time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.approaching KL150.approaching KL157.approaching KL164.approaching )
time=135 runway0.c0=135 runway0.c1=12 runway1.c0=135 runway1.c1=3 rate=0 cost=90

Delay: 3

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.approaching KL150.approaching KL157.approaching KL164.approaching )
time=138 runway0.c0=138 runway0.c1=15 runway1.c0=138 runway1.c1=6 rate=0 cost=90

Transitions:
  KL143.approaching->KL143.delayed { time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.delayed KL150.approaching KL157.approaching KL164.approaching )
time=138 runway0.c0=138 runway0.c1=15 runway1.c0=138 runway1.c1=6 rate=30 cost=90

Transitions:
  KL143.delayed->KL143.done { 1, land1!, 1 }
  runway0.loop->runway0.loop { c0 >= wait01 && c1 >= wait11, land1?, c1 := 0 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.approaching KL157.approaching KL164.approaching )
time=138 runway0.c0=138 runway0.c1=0 runway1.c0=138 runway1.c1=6 rate=0 cost=90

Delay: 2

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.approaching KL157.approaching KL164.approaching )
time=140 runway0.c0=140 runway0.c1=2 runway1.c0=140 runway1.c1=8 rate=0 cost=90

Transitions:
  KL150.approaching->KL150.delayed { time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.delayed KL157.approaching KL164.approaching )
time=140 runway0.c0=140 runway0.c1=2 runway1.c0=140 runway1.c1=8 rate=30 cost=90

Transitions:
  KL150.delayed->KL150.done { 1, land1!, 1 }
  runway1.loop->runway1.loop { c0 >= wait01 && c1 >= wait11, land1?, c1 := 0 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.done KL157.approaching KL164.approaching )
time=140 runway0.c0=140 runway0.c1=2 runway1
.c0=140 runway1.c1=0 rate=0 cost=90

Delay: 10

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.done KL157.approaching KL164.approaching )
time=150 runway0.c0=150 runway0.c1=12 runway1.c0=150 runway1.c1=10 rate=0 cost=90

Transitions:
  KL157.approaching->KL157.on_time { time >= E, land1!, 1 }
  runway1.loop->runway1.loop { c0 >= wait01 && c1 >= wait11, land1?, c1 := 0 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.done KL157.on_time KL164.approaching )
time=150 runway0.c0=150 runway0.c1=12 runway1.c0=150 runway1.c1=0 rate=30 cost=90

Transitions:
  KL157.on_time->KL157.done { time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.done KL157.done KL164.approaching )
time=150 runway0.c0=150 runway0.c1=12 runway1.c0=150 runway1.c1=0 rate=0 cost=90

Delay: 3

State:
( runway0.loop runway1.loop KL101.approaching KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.done KL157.done KL164.approaching )
time=153 runway0.c0=153 runway0.c1=15 runway1.c0=153 runway1.c1=3 rate=0 cost=90

Transitions:
  KL101.approaching->KL101.delayed { time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.delayed KL108.approaching KL115.done KL122.approaching KL129.done KL136.done 
KL143.done KL150.done KL157.done KL164.approaching )
time=153 runway0.c0=153 runway0.c1=15 runway1.c0=153 runway1.c1=3 rate=10 cost=90

Transitions:
  KL101.delayed->KL101.done { 1, land0!, 1 }
  runway0.loop->runway0.loop { c0 >= wait00 && c1 >= wait10, land0?, c0 := 0 }

State:
( runway0.loop runway1.loop KL101.done KL108.approaching KL115.done KL122.approaching KL129.done KL136.done KL143.done 
KL150.done KL157.done KL164.approaching )
time=153 runway0.c0=0 runway0.c1=15 runway1.c0=153 runway1.c1=3 rate=0 cost=90

Delay: 
27

State:
( runway0.loop
 runway1.loop KL101.done KL108.approaching KL115.done KL122.approaching KL129.done KL136.done KL143.done KL150.done 
KL157.done KL164.approaching )
time=180 runway0.c0=27 runway0.c1=42 runway1.c0=180 runway1.c1=30 rate=0 cost=90

Transitions:
  KL164.approaching->KL164.on_time { time >= E, land1!, 1 }
  runway1.loop->runway1.loop { c0 >= wait01 && c1 >= wait11, land1?, c1 := 0 }

State:
( runway0.loop runway1.loop KL101.done KL108.approaching KL115.done KL122.approaching 
KL129.done KL136.done KL143.done KL150.done KL157.done KL164.on_time )
time=180 runway0.c0=27 runway0.c1=42 runway1.c0=180 runway1.c1=0 rate=30 cost=90

Transitions:
  KL164.on_time->KL164.done { time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.done KL108.approaching KL115.done KL122.approaching KL129.
done KL136.done KL143.done KL150.done KL157.done KL164.done )
time=180 runway0.c0=27 runway0.c1=42 runway1.c0=180 runway1.c1=0 rate=0 cost=90

Delay: 16

State:
( runway0.loop runway1.loop KL101.done KL108.approaching KL115
.done KL122.approaching KL129.done KL136.done KL143.done KL150.done KL157.done KL164.done )
time=196 runway0.c0=43 runway0.c1=58 runway1.c0=196 runway1.c1=16 rate=0 cost=90

Transitions:
  KL122.approaching->KL122.on_time { time >= E, land1!, 1 }
  runway1.loop->runway1.loop { c0 >= wait01 && c1 >= wait11, land1?, c1 := 0 }

State:
( runway0
.loop runway1.loop KL101.done KL108.approaching KL115.done KL122.on_time KL129.done KL136.done KL143.done KL150.done 
KL157.done KL164.done )
time=196 runway0.c0=43 runway0.c1=58 runway1.c0=196 runway1.c1=0 rate=30 cost=90

Transitions:
  KL122.on_time->KL122.done { time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.done KL108.approaching KL115.done KL122.done KL129.done KL136
.done KL143.done KL150.done KL157.done KL164.done )
time=196 runway0.c0=43 runway0.c1=58 runway1.c0=196 runway1.c1=0 rate=0 cost=90

Delay: 62


State:
( runway0.loop runway1.loop KL101.done KL108.approaching KL115.done KL122.done KL129.done KL136.done KL143.done KL150
.done KL157.done KL164.done )
time=258 runway0.c0=105 runway0.c1=120 runway1.c0=258 runway1.c1=62 rate=0 cost=90

Transitions:
  KL108.approaching->KL108.on_time { time >= E, land0!, 1 }
  runway0.loop->runway0.loop { c0 >= wait00 && c1 >= wait10, land0?, c0 := 0 }

State:
( runway0.loop runway1.loop KL101.done KL108.on_time KL115.done KL122.done KL129.done 
KL136.done KL143.done KL150.done KL157.done KL164.done )
time=258 runway0.c0=0 runway0.c1=120 runway1.c0=258 runway1.c1=62 rate=10 cost=90

Transitions:
  KL108.on_time->KL108.done { 
time == T, tau, 1 }

State:
( runway0.loop runway1.loop KL101.done KL108.done KL115.done KL122.done KL129.done KL136.done KL143.done KL150.done 
KL157.done KL164
.done )
time=258 runway0.c0=0 runway0.c1=120 runway1.c0=258 runway1.c1=62 rate=0 cost=90

