.\verifyta.exe : 
At line:1 char:1
+ .\verifyta.exe -t3 .\2batches.xml .\2batches.q 1>info-2b.txt 2>output ...
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:String) [], RemoteException
    + FullyQualifiedErrorId : NativeCommandError
 
State:
( ProdA.waitSetupBio ProdB.waitSetupBio Bio1.Idle Bio2.Idle Pur1.
Idle 
Pur2.Idle
 
Clean.
Idle 
)
ProdA.cA
=0
 ProdB.cB
=0 Bio1.cb1
=0
 Bio2.cb2
=0
 Pur1.cp1
=0
 Pur2.cp2
=0
 Clean.cC
=0
 rate=0
 cost=0


Transitions:

  ProdB
.waitSetupBio
->ProdB
.
execSetupBio2j { 
1
, sSetupBio2!
, cB := 0 }

  
Bio2
.Idle
->Bio2
.execSetupBio
 { 1
, sSetupBio2?
, cb2 := 0
 }


State:

( ProdA
.waitSetupBio
 ProdB
.execSetupBio2j
 Bio1
.Idle
 Bio2
.execSetupBio
 Pur1
.Idle 
Pur2.
Idle 
Clean.
Idle 
)
ProdA.cA=
0 
ProdB.cB=
0 
Bio1.cb1=
0 Bio2.cb2
=0
 Pur1.cp1
=0
 Pur2.cp2
=0
 Clean.cC
=0
 rate=
0 cost=0


Delay: 
10


State:
( ProdA.waitSetupBio ProdB.execSetupBio2j Bio1.Idle Bio2.
execSetupBio 
Pur1.Idle
 Pur2
.Idle 
Clean.
Idle 
)
ProdA.cA=
10 
ProdB.cB=
10 
Bio1.cb1=
10 
Bio2.cb2=
10 
Pur1.cp1=
10 
Pur2.cp2=10
 Clean.cC
=10
 rate=
0 cost=
0


Transitions:
  
ProdA.
waitSetupBio->
ProdA.
execSetupPur1f { 1
, sSetupPur1!
, cA := 0
 }

  Pur1
.Idle
->Pur1
.execSetupPur
 { 1
, sSetupPur1?
, cp1 := 0 }



State:
( 
ProdA.
execSetupPur1f 
ProdB.
execSetupBio2j 
Bio1.
Idle 
Bio2.
execSetupBio Pur1
.execSetupPur
 Pur2
.Idle
 Clean
.Idle
 )

ProdA.cA=0 ProdB.cB=10 Bio1.cb1=10 Bio2.cb2=10 Pur1.cp1=0 Pur2.cp2=10 Clean.cC=10 rate=0 cost=0

Transitions:
  Bio2.execSetupBio->Bio2.waitFerm { 1, eSetupBio2!, cb2 := 0 }
  ProdB.execSetupBio2j->ProdB.waitFerm2j { cB >= t12ss, eSetupBio2?, cB := 0 }

State:
( ProdA.execSetupPur1f ProdB.waitFerm2j Bio1.Idle Bio2.waitFerm Pur1.execSetupPur Pur2.Idle Clean.Idle )
ProdA.cA=0 ProdB.cB=0 Bio1.cb1=10 Bio2.cb2=0 Pur1.cp1=0 Pur2.cp2=10 Clean.cC=10 rate=0 cost=0

Delay: 10

State:
( ProdA.execSetupPur1f ProdB.waitFerm2j Bio1.Idle Bio2.waitFerm Pur1.execSetupPur Pur2.Idle Clean.Idle )
ProdA.cA=10 ProdB.cB=10 Bio1.cb1=20 Bio2.cb2=10 Pur1.cp1=10 Pur2.cp2=20 Clean.cC=20 rate=0 cost=0

Transitions:
  ProdB.waitFerm2j->ProdB.execFerm2j { cB >= t12sf1, sFerm2!, cB := 0 }
  Bio2.waitFerm->Bio2.execFerm { 1, sFerm2?, cb2 := 0 }

State:
( ProdA.execSetupPur1f ProdB.execFerm2j Bio1.Idle Bio2.execFerm Pur1.execSetupPur Pur2.Idle Clean.Idle )
ProdA.cA=10 ProdB.cB=
0 Bio1.cb1=20 Bio2.cb2=0 Pur1.cp1
=10 
Pur2.cp2=20 Clean.cC=20 rate=0 cost=0

Delay: 40

State:
( ProdA.execSetupPur1f ProdB.execFerm2j Bio1.Idle
 Bio2.execFerm Pur1.execSetupPur Pur2.Idle Clean.Idle )
ProdA.cA=50 ProdB.cB=40 Bio1.cb1=60 Bio2.cb2=40 Pur1.cp1=50 Pur2.cp2=60 Clean.cC=60 rate=0 cost=0

Transitions:
  Pur1.execSetupPur->Pur1.waitPur { 1, tau, cp1 := 0 }

State:
( ProdA.execSetupPur1f ProdB.execFerm2j Bio1.Idle Bio2.execFerm Pur1.waitPur Pur2.Idle Clean.Idle )
ProdA.cA=50 ProdB.cB=40 Bio1.cb1=60 Bio2.cb2=40 Pur1.cp1=0 Pur2.cp2=60 Clean.cC=60 rate=0 cost=0

Transitions:
  ProdB.execFerm2j->ProdB.execSetupPur2j { cB >= t12f1 && cB >= t22ss, sSetupPur2!, cB := 0 }
  Pur2.Idle->Pur2.execSetupPur { 1, sSetupPur2?, cp2 := 0 }

State:
( ProdA.execSetupPur1f ProdB.execSetupPur2j Bio1.Idle Bio2.execFerm Pur1.waitPur Pur2.execSetupPur Clean.Idle )
ProdA.cA=50 ProdB.cB=0 Bio1.cb1=60 Bio2.cb2=40 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=60 rate=0 cost=0

Transitions:
  ProdA.execSetupPur1f->ProdA.execSetupbio1f { 1, sSetupBio1!, 1 }
  Bio1.Idle->Bio1.execSetupBio { 1, sSetupBio1?, cb1 := 0 }

State:
( ProdA.execSetupbio1f ProdB.execSetupPur2j Bio1.execSetupBio Bio2.execFerm Pur1.waitPur Pur2.execSetupPur Clean.Idle )
ProdA.cA=50 ProdB.cB=0 Bio1.cb1=0 Bio2.cb2=40 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=60 rate=0 cost=0

Transitions:
  Bio1.execSetupBio->Bio1.waitFerm { 1, eSetupBio1!, cb1 := 0 }
  ProdA.execSetupbio1f->ProdA.waitFerm1f { cA >= t11ss, eSetupBio1?, cA := 0 }

State:
( ProdA.waitFerm1f ProdB.execSetupPur2j Bio1.waitFerm Bio2.execFerm Pur1.waitPur Pur2.execSetupPur Clean.Idle )
ProdA.cA=0 ProdB.cB=0 Bio1.cb1=0 Bio2.cb2=40 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=60 rate=0 cost=0

Delay: 10

State:
( ProdA.waitFerm1f ProdB.execSetupPur2j Bio1.waitFerm Bio2.execFerm Pur1.waitPur Pur2.execSetupPur Clean.Idle )
ProdA.cA=10 ProdB.cB=10 Bio1.cb1=10 Bio2.cb2=50 Pur1.cp1=10 Pur2.cp2=10 Clean.cC=70 rate=0 cost=0

Transitions:
  ProdA.waitFerm1f->ProdA.execFerm1f { cA >= t11sf1, sFerm1!, cA := 0 }
  Bio1.waitFerm->Bio1.execFerm { 1, sFerm1?, cb1 := 0 }

State:
( ProdA.execFerm1f ProdB.execSetupPur2j Bio1.execFerm Bio2.execFerm Pur1.waitPur Pur2.execSetupPur Clean.Idle )
ProdA.cA=0 ProdB.cB=10 Bio1.cb1=0 Bio2.cb2=50 Pur1.cp1=10 Pur2.cp2=10 Clean.cC=70 rate=0 cost=0

Delay: 29

State:
( ProdA.execFerm1f ProdB.execSetupPur2j Bio1.execFerm Bio2.execFerm Pur1.waitPur Pur2.execSetupPur Clean.Idle )
ProdA.cA=29 ProdB.cB=39 Bio1.cb1=29 Bio2.cb2=79 Pur1.cp1=39 Pur2.cp2=39 Clean.cC=99 rate=0 cost=0

Transitions:
  Bio2.execFerm->Bio2.execPur { 1, eFerm2!, cb2 := 0 }
  ProdB.execSetupPur2j->ProdB.waitPur2j { 1, eFerm2?, cB := 0 }

State:
( ProdA.execFerm1f ProdB.waitPur2j Bio1.execFerm Bio2.execPur Pur1.waitPur Pur2.execSetupPur Clean.Idle )
ProdA.cA=29 ProdB.cB=0 Bio1.cb1=29 Bio2.cb2=0 Pur1.cp1=39 Pur2.cp2=39 Clean.cC=99 rate=0 cost=0

Transitions:
  Pur2.execSetupPur->Pur2.waitPur { 1, tau, cp2 := 0 }

State:
( ProdA.execFerm1f ProdB.waitPur2j Bio1.execFerm Bio2.execPur Pur1.waitPur Pur2.waitPur Clean.Idle )
ProdA.cA=29 ProdB.cB=0 Bio1.cb1=29 Bio2.cb2=0 Pur1.cp1=39 Pur2.cp2=0 Clean.cC=99 rate=0 cost=0

Transitions:
  Bio2.execPur->Bio2.waitCleanBio { 1, sPur2!, 1 }
  Pur2.waitPur->Pur2.execPur { 1, sPur2?, cp2 := 0 }

State:
( ProdA.execFerm1f ProdB.waitPur2j Bio1.execFerm Bio2.waitCleanBio Pur1.waitPur Pur2.execPur Clean.Idle )
ProdA.cA=29 ProdB.cB=0 Bio1.cb1=29 Bio2.cb2=0 Pur1.cp1=39 Pur2.cp2=0 Clean.cC=99 rate=0 cost=0

Transitions:
  Bio2.waitCleanBio->Bio2.execCleanBio { 1, sCleanBio2!, cb2 := 0 }
  Clean.Idle->Clean.waitCleanBio2 { 1, sCleanBio2?, cC := 0 }

State:
( ProdA.execFerm1f ProdB.waitPur2j Bio1.execFerm Bio2.execCleanBio Pur1.waitPur Pur2.execPur Clean.waitCleanBio2 )
ProdA.cA=29 ProdB.cB=0 Bio1.cb1=29 Bio2.cb2=0 Pur1.cp1=39 Pur2.cp2=0 Clean.cC=0 rate=0 cost=0

Delay: 11

State:
( ProdA.execFerm1f ProdB.waitPur2j Bio1.execFerm Bio2.execCleanBio Pur1.waitPur Pur2.execPur Clean.waitCleanBio2 )
ProdA.cA=40 ProdB.cB=11 Bio1.cb1=40 Bio2.cb2=11 Pur1.cp1=50 Pur2.cp2=11 Clean.cC=11 rate=0 cost=0

Transitions:
  Bio1.execFerm->Bio1.execPur { 1, eFerm1!, cb1 := 0 }
  ProdA.execFerm1f->ProdA.waitPur1f { 
cA >= t11f1 && cA >= t21ss, eFerm1?, cA := 0 }


State:
( 
ProdA.waitPur1f
 ProdB.
waitPur2j Bio1.execPur Bio2.execCleanBio Pur1.waitPur Pur2.execPur Clean.waitCleanBio2 )
ProdA.cA=0 ProdB.cB=11 Bio1.cb1=0 Bio2.cb2=11 Pur1.cp1=50 Pur2.cp2=11 Clean.cC=11 rate=0 cost=0

Transitions:
  ProdB
.waitPur2j->ProdB.execPur2 { cB <= t12f1c, tau, 1 }

State:
( ProdA.waitPur1f ProdB.execPur2 Bio1.execPur Bio2.execCleanBio Pur1.waitPur Pur2.execPur Clean.waitCleanBio2 )
ProdA.cA=0 ProdB.cB=11 Bio1.cb1=0 Bio2.cb2=11 
Pur1.cp1=50 Pur2.cp2=11 Clean.cC=11 rate=0 cost=0

Transitions:
  Clean.waitCleanBio2->Clean.execCleanBio2 { cC >= t12f1c, tau, cC := 0 }

State:
( ProdA.waitPur1f ProdB.execPur2 Bio1.execPur Bio2.execCleanBio Pur1.waitPur Pur2.execPur Clean.execCleanBio2
 )
ProdA.cA=0 ProdB.cB=11 Bio1.cb1=0 Bio2.cb2=11 Pur1.cp1=50 Pur2.cp2=11 Clean.cC=0 rate=0 cost=0

Delay: 11

State:
( ProdA.waitPur1f ProdB.execPur2
 Bio1.execPur Bio2.execCleanBio Pur1.waitPur Pur2.execPur Clean.execCleanBio2 )
ProdA.cA=11 ProdB.cB=22 Bio1.cb1=11 Bio2.cb2=22 Pur1.cp1=61 Pur2.cp2=22 Clean.cC=11 rate=0 cost=0

Transitions:
  Pur2.execPur->Pur2.waitCleanPur { 
1, ePur2!, cp2 := 0 }
  ProdB.execPur2->ProdB.ENDJOB { cB >= t22f2, ePur2?, cB := 0 }

State:
( ProdA.waitPur1f ProdB.ENDJOB Bio1.execPur Bio2.execCleanBio Pur1.waitPur Pur2.waitCleanPur Clean.execCleanBio2 )
ProdA.cA=11 ProdB.cB=0
 Bio1.cb1=11 Bio2.cb2=22 Pur1.cp1=61 Pur2.cp2=0 Clean.cC=11 rate=0 cost=0

Transitions:
  Bio1.execPur->Bio1.waitCleanBio { 1, sPur1!, 1 }
  Pur1.waitPur->Pur1.execPur { 1, sPur1?, cp1 := 0 }

State:
( ProdA
.waitPur1f ProdB.ENDJOB Bio1.waitCleanBio Bio2.execCleanBio Pur1.execPur Pur2.waitCleanPur Clean.execCleanBio2 )
ProdA.cA=11 ProdB.cB=0 Bio1.cb1=11 Bio2.cb2=22 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=11
 rate=0 cost=0

Transitions:
  ProdA.waitPur1f->ProdA.execPur1 { cA <= t11f1c, tau, 1 }

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.waitCleanBio Bio2.execCleanBio Pur1.execPur Pur2.waitCleanPur Clean.execCleanBio2 )

ProdA.cA=11 ProdB.cB=0 Bio1.cb1=11 Bio2.cb2=22 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=11 rate=0 cost=0

Transitions:
  Clean.execCleanBio2->Clean.Idle { cC >= t12c, eCleanBio2!, cC := 0 }
  Bio2
.execCleanBio->Bio2.Idle { 1, eCleanBio2?, cb2 := 0 }

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.waitCleanBio Bio2.Idle Pur1.execPur Pur2.waitCleanPur Clean.Idle )
ProdA.cA=11 ProdB.cB=0 Bio1.cb1=11
 Bio2.cb2=0 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=0 rate=0 cost=0

Transitions:
  Bio1.waitCleanBio->Bio1.execCleanBio { 1, sCleanBio1!, cb1 := 0 }
  Clean.Idle->Clean.waitCleanBio1 { 1, sCleanBio1?, cC := 0 }

State:
( ProdA.
execPur1 ProdB.ENDJOB Bio1.execCleanBio Bio2.Idle Pur1.execPur Pur2.waitCleanPur Clean.waitCleanBio1 )
ProdA.cA=11 ProdB.cB=0 Bio1.cb1=0 Bio2.cb2=0 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=0 rate=0 cost=0


Transitions:
  Clean.waitCleanBio1->Clean.execCleanBio1 { cC <= t11f1c, tau, cC := 0 }

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.execCleanBio Bio2.Idle Pur1.execPur Pur2.waitCleanPur Clean.execCleanBio1 )
ProdA.cA=11 ProdB.cB=0
 Bio1.cb1=0 Bio2.cb2=0 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=0 rate=0 cost=0

Delay: 10

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.
execCleanBio Bio2.Idle Pur1.execPur Pur2.waitCleanPur Clean.execCleanBio1 )
ProdA.cA=21 ProdB.cB=10 Bio1.cb1=10 Bio2.cb2=10 Pur1.cp1=10 Pur2.cp2=10 Clean.cC=10 rate=0 cost=0

Transitions:
  Clean.execCleanBio1->Clean.
Idle { cC >= t11c, eCleanBio1!, cC := 0 }
  Bio1.execCleanBio->Bio1.Idle { 1, eCleanBio1?, cb1 := 0 }

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.execPur Pur2.waitCleanPur 
Clean.Idle )
ProdA.cA=21 ProdB.cB=10 Bio1.cb1=0 Bio2.cb2=10 Pur1.cp1=10 Pur2.cp2=10 Clean.cC=0 rate=0 cost=0

Delay: 11

State:
( ProdA.execPur1 ProdB.ENDJOB
 Bio1.Idle Bio2.Idle Pur1.execPur Pur2.waitCleanPur Clean.Idle )
ProdA.cA=32 ProdB.cB=21 Bio1.cb1=11 Bio2.cb2=21 Pur1.cp1=21 Pur2.cp2=21 Clean.cC=11 rate=0 cost=0

Transitions:
  
Pur2.waitCleanPur->Pur2.execCleanPur { 1, sCleanPur2!, cp2 := 0 }
  Clean.Idle->Clean.waitCleanPur2 { 1, sCleanPur2?, 1 }

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.execPur
 Pur2.execCleanPur Clean.waitCleanPur2 )
ProdA.cA=32 ProdB.cB=21 Bio1.cb1=11 Bio2.cb2=21 Pur1.cp1=21 Pur2.cp2=0 Clean.cC=11 rate=0 cost=0

Transitions:
  Clean.waitCleanPur2->Clean.
execCleanPur2 { cC >= t22f2c, tau, cC := 0 }

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.execPur Pur2.execCleanPur Clean.execCleanPur2 )
ProdA.cA=32 ProdB.cB=21 Bio1.cb1=11 Bio2.cb2=21 Pur1.cp1=21 
Pur2.cp2=0 Clean.cC=0 rate=0 cost=0

Delay: 11

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.execPur Pur2.execCleanPur Clean.execCleanPur2 )
ProdA.cA=43 ProdB.cB=32 Bio1.cb1=22 Bio2.cb2=32 
Pur1.cp1=32 Pur2.cp2=11 Clean.cC=11 rate=0 cost=0

Transitions:
  Clean.execCleanPur2->Clean.Idle { cC >= t22c, eCleanPur2!, cC := 0 }
  Pur2.execCleanPur->Pur2.Idle { 1, eCleanPur2?, cp2 := 0 }

State:
( ProdA.execPur1 ProdB.ENDJOB Bio1.Idle 
Bio2.Idle Pur1.execPur Pur2.Idle Clean.Idle )
ProdA.cA=43 ProdB.cB=32 Bio1.cb1=22 Bio2.cb2=32 Pur1.cp1=32 Pur2.cp2=0 Clean.cC=0 rate=0 cost=0

Transitions:
  Pur1.execPur->Pur1.waitCleanPur { 1, ePur1!, cp1 := 0 }

  ProdA.execPur1->ProdA.ENDJOB { cA >= t21f2, ePur1?, cA := 0 }

State:
( ProdA.ENDJOB ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.waitCleanPur Pur2.Idle Clean.Idle )
ProdA.cA=0 ProdB.cB=32 Bio1.cb1=22 Bio2.cb2=32 Pur1.cp1=0 
Pur2.cp2=0 Clean.cC=0 rate=0 cost=0

Transitions:
  Pur1.waitCleanPur->Pur1.execCleanPur { 1, sCleanPur1!, cp1 := 0 }
  Clean.Idle->Clean.waitCleanPur1 { 1, sCleanPur1?, cC := 0 }

State:
( ProdA.ENDJOB ProdB.ENDJOB Bio1.Idle Bio2.Idle 
Pur1.execCleanPur Pur2.Idle Clean.waitCleanPur1 )
ProdA.cA=0 ProdB.cB=32 Bio1.cb1=22 Bio2.cb2=32 Pur1.cp1=0 Pur2.cp2=0 Clean.cC=0 rate=0 cost=0

Delay: 10

State:
( ProdA.ENDJOB ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.execCleanPur Pur2.Idle Clean.
waitCleanPur1 )
ProdA.cA=10 ProdB.cB=42 Bio1.cb1=32 Bio2.cb2=42 Pur1.cp1=10 Pur2.cp2=10 Clean.cC=10 rate=0 cost=0

Transitions:
  Clean.waitCleanPur1->Clean.execCleanPur1 { cC >= t21f2c, tau, cC := 0 }

State:
( ProdA.ENDJOB ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.execCleanPur Pur2.Idle
 Clean.execCleanPur1 )
ProdA.cA=10 ProdB.cB=42 Bio1.cb1=32 Bio2.cb2=42 Pur1.cp1=10 Pur2.cp2=10 Clean.cC=0 rate=0 cost=0

Delay: 10

State:
( ProdA.ENDJOB ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.execCleanPur Pur2.Idle
 Clean.execCleanPur1 )
ProdA.cA=20 ProdB.cB=52 Bio1.cb1=42 Bio2.cb2=52 Pur1.cp1=20 Pur2.cp2=20 Clean.cC=10 rate=0 cost=0

Transitions:
  Clean.execCleanPur1->Clean.Idle { cC >= t21c, eCleanPur1!, cC := 0 }
  Pur1.execCleanPur->Pur1.Idle { 1, eCleanPur1?, cp1 := 0 }

State:

( ProdA.ENDJOB ProdB.ENDJOB Bio1.Idle Bio2.Idle Pur1.Idle Pur2.Idle Clean.Idle )
ProdA.cA=20 ProdB.cB=52 Bio1.cb1=42 Bio2.cb2=52 Pur1.cp1=0 Pur2.cp2=20 Clean.cC=0 rate=0 cost=0

