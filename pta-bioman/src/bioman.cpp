/*
 * main.cpp
 *
 *  Created on: Feb 28, 2015
 *      Author: devendra
 *  Updated on: Oct 06, 2018
 *      Author: Mohammad Hekmatnejad
 */

//using namespace std;

#include "TimedAutomaton/prjHeaders.h"
#include "TimedAutomaton/prjParams.h"

#include <cstdlib>
#include <clocale>

int findBitvectorLength(int numLoc) {
	int size = numLoc;
	if(size == 0) return 1;

	// Calculate length of bitvector
	int bvlen = 0;
	if(size == 1) {
		bvlen = 1;
	}
	else {
		size --;
		while(size > 0) {
			bvlen ++;
			size = size >> 1;
		}
	}
	return bvlen;
}

int ta_smt(int argc, char* argv[]) {
	CHAR filename[BUFFER_SIZE];
	SNPRINTF(filename, BUFFER_SIZE, STR("%s"), argv[2]);

	CHAR reach_loc_name[BUFFER_SIZE];
	SNPRINTF(reach_loc_name, BUFFER_SIZE, STR("%s"), argv[3]);

	int bound = atoi(argv[4]);

	TimedAutomaton ta;
	ta.loadFromFile(String(filename));
        //ta = makeAutomata();
        //COUT<< "using a mock PTA...\n";
        COUT<< ta.toString()<<"\n";        

	int bvlen = findBitvectorLength(ta.getLocationsCount());

	ta.generateSmtFormula(bound, true);

	// Reachability condition
	CHAR buf[BUFFER_SIZE];
	SNPRINTF(buf, BUFFER_SIZE, STR("(assert (= %S_s_%d (_ bv%d %d)))\n"), ta.getName().c_str(), bound, ta.getLocationIndex(reach_loc_name), bvlen);

	COUT<<buf;
	COUT<<STR("(check-sat)\n");
	COUT<<STR("(get-model)\n");

	return 0;
}

int pta_smt(int argc, char* argv[]) {
	CHAR filename[BUFFER_SIZE];
	SNPRINTF(filename, BUFFER_SIZE, STR("%s"), argv[2]);

	CHAR reach_loc_name[BUFFER_SIZE];
	SNPRINTF(reach_loc_name, BUFFER_SIZE, STR("%s"), argv[3]);

	// TODO: price expression in argv[5]

	int bound = atoi(argv[4]);

	PricedTimedAutomaton pta;
	pta.loadFromFile(String(filename));

	int bvlen = findBitvectorLength(pta.getLocationsCount());

	pta.generateSmtFormula(bound, true);

	// Reachability condition
	CHAR buf[BUFFER_SIZE];
	SNPRINTF(buf, BUFFER_SIZE, STR("(assert (= %S_s_%d (_ bv%d %d)))\n"), pta.getName().c_str(), bound, pta.getLocationIndex(reach_loc_name), bvlen);

	COUT<<buf;
	COUT<<STR("(check-sat)\n");
	COUT<<STR("(get-model)\n");

	return 0;
}

void ppta_smt(int argc, char* argv[]) {
	CHAR filename[BUFFER_SIZE];
	SNPRINTF(filename, BUFFER_SIZE, STR("%s"), argv[2]);

	CHAR reach_loc_name[BUFFER_SIZE];
	SNPRINTF(reach_loc_name, BUFFER_SIZE, STR("%s"), argv[3]);

	// TODO: price expression in argv[4]

	int bound = atoi(argv[4]);
        
	PolyPricedTimedAutomaton pa;
	pa.loadFromFile(filename);
	//pa.generateSmtFormula(2);
	//Polynomial p(STR("(2)t^(2)+(1)t^(1)+(-1)t^(0)"));
	//Polynomial p(STR("(2)t^(2)+(-3)t^(1)+(4)t^(0)"));
	//COUT<<p.toSmtString(STR("D_123"));
	//COUT << pa.toString();
	int bvlen = findBitvectorLength(pa.getLocationsCount());
	pa.generateSmtFormula(bound, true);

	// Reachability condition
	CHAR buf[BUFFER_SIZE];
	SNPRINTF(buf, BUFFER_SIZE, STR("(assert (= %S_s_%d (_ bv%d %d)))\n"), pa.getName().c_str(), bound, pa.getLocationIndex(reach_loc_name), bvlen);

	COUT<<buf;
	COUT<<STR("(check-sat)\n");
	COUT<<STR("(get-model)\n");

        
}

void ccs_ppta_smt_iterative(int argc, char* argv[], bool isDreal) {
    
    int num_files = (argc-3)/2;
    string *str_file_name = new string[num_files];
    string *str_prc_name = new string[num_files];
    String *str_target_name = new String[num_files];
    bool *isTargetPrice = new bool[num_files];
    bool *isTarget = new bool[num_files];
    PolyPricedTimedAutomaton pptas[num_files];
    //todo: NEED TO BE CHANGED
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
    int numNoTargetPrice = 0;
    int numNoTarget = 0;
    
    for(int i=0; i<num_files; i++){
        str_prc_name[i] = argv[3+i*2];
        isTarget[i] = true;
        if(str_prc_name[i].at(0)=='-')//if process name starts with '-' we don't put it as reachable target
        {
            isTarget[i] = false;
            numNoTarget++;
            str_prc_name[i] = str_prc_name[i].substr(1,str_prc_name[i].length()-1);
        }
        str_file_name[i] = str_prc_name[i].append(".xml");
    }
    for(int i=0; i<num_files; i++){
        string targetState = argv[4+i*2];
        str_target_name[i] = converter.from_bytes(argv[4+i*2]);
        isTargetPrice[i] = true;
        if(targetState.at(0)=='-')//if target location starts with '-' we don't want the cost to be calculated for it
        {
            isTargetPrice[i] = false;
            numNoTargetPrice++;
            str_target_name[i] = str_target_name[i].substr(1,str_target_name[i].length()-1);
        }
    }
    
    std::wstreambuf *coutbuf = std::wcout.rdbuf(); //save old buf
    
    for(int i=0; i< num_files; i++){
        String wfn = converter.from_bytes(str_file_name[i]);
        std::wcout.rdbuf(coutbuf);
        pptas[i].loadFromFile(wfn);
        pptas[i].type = TimedAutomaton::e_type::PPTA;
        if(isDreal)
            pptas[i].useBitVector = false;//in order to use dReal rather than Z3
        else
            pptas[i].useBitVector = true;//in order to use Z3
        //ignore the cost for some with target name start with '-'
        pptas[i].setIgnorePriceGeneration(!isTargetPrice[i]);
        COUT<< wfn<<endl<<pptas[i].toString() << endl<<STR("===============\n");
      
        std::wofstream out(str_file_name[i] + ".txt");
        std::wcout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
        //write a name_lookup_map into a file
        for(int j=0; j<pptas[i].locations.size(); j++)
            COUT << STR("s.") << j << STR(" ") << pptas[i].locations.at(j).name<< endl;
        for(int j=0; j<pptas[i].transitions.size(); j++)
            COUT << STR("T.") << j << STR(" ") << pptas[i].locations.at(pptas[i].transitions.at(j).src).name<< STR(" -> ") 
                    << pptas[i].locations.at(pptas[i].transitions.at(j).dest).name<< endl;
        for(int j=0; j<pptas[i].alphabets.size(); j++)
            COUT << STR("A.") << j << STR(" ") << pptas[i].alphabets.at(j)<< endl;
        //for(int j=0; j<pptas[i].updates.size(); j++)
        //    COUT << STR("U.") << j << STR(" ") << pptas[i].updates.at(j).name<< endl;
    }


        int bound = stoi(argv[2]);
        //bound += 4;
        
        //data for z3 solver and post-processing
        std::wofstream solverout("solver.txt");
        std::wcout.rdbuf(solverout.rdbuf()); //redirect std::cout to out.txt!
        COUT<<STR("MAX_STEP =")<<bound<<endl;
        COUT<<STR("NUM_PRC =")<<(num_files)<<endl;
        COUT<<STR("NUM_IGNORED_PRC =")<<(numNoTarget)<<endl;
        COUT<<STR("NUM_IGNORED_PRC_PRICE =")<<(numNoTargetPrice)<<endl;
        for(int k=0; k< num_files; k++)
            if(!isTargetPrice[k])
            COUT<<STR("IGNORED_PRC_PRICE =")<<converter.from_bytes(str_prc_name[k])<<endl;
        COUT<<STR("Z3_TIME_OUT =200000")<<endl;
        COUT<<STR("Z3_OPTIMIZATION_TIME_OUT =6000000")<<endl;
        COUT<<STR("Z3_TIME_OUT_INCREASE =200000")<<endl;
        COUT<<STR("MAX_Z3_BISECTION_TIME_OUT =2000000")<<endl;
        COUT<<STR("MONOTONICALLY_INCREASING=false")<<endl;
        COUT<<STR("PRINT_DEBUG_INFO=false")<<endl;
        
    
        std::wofstream out("code.smt2");//translated file name
        std::wcout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!

	TimedSystem tsys;
        Idx ppta_idxs[num_files];
        int bvlens[num_files];

        for(int i=0; i< num_files; i++){
            ppta_idxs[i] = tsys.addAutomaton(&pptas[i]);
            String wfn = converter.from_bytes(str_file_name[i]);
            CCSProcess p(wfn, ppta_idxs[i]);
            tsys.addProcess(p);
            bvlens[i] = findBitvectorLength(pptas[i].getLocationsCount());
        }

        for(int iteration=1; iteration<= bound; iteration++){
            tsys.generateSmtFormulaSingleStep(iteration, bound);
        }
        
        // Reachability condition
        COUT<<STR("\n\n;;;===>>>Reachability Constraint\n\n");
	CHAR buf[BUFFER_SIZE];
        for(int i=0; i<num_files; i++){
            if(isTarget[i]){
                if(pptas[i].useBitVector)
                    SNPRINTF(buf, BUFFER_SIZE, STR("(assert (= %S_s_%d (_ bv%d %d)))\n"), pptas[i].getName().c_str(), 
                            bound, pptas[i].getLocationIndex(str_target_name[i]), bvlens[i]);
                else
                    SNPRINTF(buf, BUFFER_SIZE, STR("(assert (= %S_s_%d %d))\n"), pptas[i].getName().c_str(), 
                            bound, pptas[i].getLocationIndex(str_target_name[i]));
                COUT<<buf;
            }
        }
        String strSumPrice;
        if(numNoTargetPrice<num_files){
            COUT<<STR("\n\n;;;===>>>Optimization Constraint\n\n");
            String optStr = STR("(minimize (+ ");
            for(int i=0; i<num_files; i++){
                if(isTargetPrice[i])
                    strSumPrice += pptas[i].getName()+STR("_price_") + ::toString(bound) + STR(" ");
            }
            optStr += strSumPrice;
            optStr += STR("))\n");
            COUT<<optStr;
        }
        COUT<<STR("\n\n;;;===>>>Satisfiability Check\n\n");
        COUT<<STR(";(set-option :timeout 6000000)\n");
/*
 https://rise4fun.com/Z3/tutorial/optimization
(set-option :opt.priority pareto)
;(set-option :opt.priority box)
;(set-option :smt.arith.solver 1)
(set-option :opt.wmaxsat_engine hsmax)
;(set-option :opt.enable_sat true)
;(set-option :opt.elim_01 false)
;(set-option :smt.pb.conflict_frequency 1)
;(set-option :smt.pb.enable_compilation false)
;(set-option :smt.pb.enable_simplex true)
 * 
 * used ones
(set-option :parallel.enable true)
(check-sat-using
 (par-or
  ;; Strategy 1: using seed 1
  (using-params smt :random-seed 1)
  ;; Strategy 1: using seed 2
  (using-params smt :random-seed 2)
  ;; Strategy 1: using seed 3
  (using-params smt :random-seed 3)
  ;; Strategy 1: using seed 4
  (using-params smt :random-seed 4)))  
*/        

        COUT<<STR(";(set-option :opt.priority pareto)\n");
        COUT<<STR("(check-sat)\n");
        COUT<<STR("(get-info :all-statistics)\n");

        COUT<< STR("(echo \"Objectives:\")\n");
        for(int i=0; i<num_files && numNoTargetPrice<num_files; i++){
            if(isTargetPrice[i])
                COUT<< STR("(get-value (") + pptas[i].getName()+STR("_price_") + ::toString(bound) + STR("))") <<endl;
        }            
        if(numNoTargetPrice<num_files){
            COUT<< STR("(echo \"Total sum:\")\n");
            COUT<< STR("(eval (+ ")+strSumPrice+STR("))\n");
        }
}

void ccs_ta_smt_iterative(int argc, char* argv[]) {
    
    int num_files = (argc-3)/2;
    string *str_file_name = new string[num_files];
    string *str_prc_name = new string[num_files];
    String *str_target_name = new String[num_files];
    bool *isTarget = new bool[num_files];
    TimedAutomaton pptas[num_files];
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
    int numNoTarget = 0;
    
    for(int i=0; i<num_files; i++){
        str_file_name[i] = argv[3+i*2];
        str_file_name[i].append(".xml");
        str_prc_name[i] = argv[3+i*2];
    }
    for(int i=0; i<num_files; i++){
        string tarLoc = argv[4+i*2];
        isTarget[i] = true;
        str_target_name[i] = converter.from_bytes(argv[4+i*2]);

        if(tarLoc.at(0)=='-')//if target location starts with '-' we don't want the cost to be calculated for it
        {
            isTarget[i] = false;
            numNoTarget++;
            str_target_name[i] = converter.from_bytes(argv[4+i*2]);
            str_target_name[i] = str_target_name[i].substr(1,str_target_name[i].length()-1);
        }
    }
    
    std::wstreambuf *coutbuf = std::wcout.rdbuf(); //save old buf
    
    for(int i=0; i< num_files; i++){
        String wfn = converter.from_bytes(str_file_name[i]);
        std::wcout.rdbuf(coutbuf);
        pptas[i].loadFromFile(wfn);
        pptas[i].type = TimedAutomaton::e_type::TA;
        //ignore the cost for some with target name start with '-'
        //pptas[i].setIgnorePriceGeneration(!isTarget[i]);
        COUT<< wfn<<endl<<pptas[i].toString() << endl<<STR("===============\n");
      
        std::wofstream out(str_file_name[i] + ".txt");
        std::wcout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
        //write a name_lookup_map into a file
        for(int j=0; j<pptas[i].locations.size(); j++)
            COUT << STR("s.") << j << STR(" ") << pptas[i].locations.at(j).name<< endl;
        for(int j=0; j<pptas[i].transitions.size(); j++)
            COUT << STR("T.") << j << STR(" ") << pptas[i].locations.at(pptas[i].transitions.at(j).src).name<< STR(" -> ") 
                    << pptas[i].locations.at(pptas[i].transitions.at(j).dest).name<< endl;
        for(int j=0; j<pptas[i].alphabets.size(); j++)
            COUT << STR("A.") << j << STR(" ") << pptas[i].alphabets.at(j)<< endl;
        for(int j=0; j<pptas[i].updates.size(); j++)
            COUT << STR("U.") << j << STR(" ") << pptas[i].updates.at(j).name<< endl;
    }


        int bound = stoi(argv[2]);
        //bound += 4;
        
        //data for z3 solver and post-processing
        std::wofstream solverout("solver.txt");
        std::wcout.rdbuf(solverout.rdbuf()); //redirect std::cout to out.txt!
        COUT<<STR("MAX_STEP =")<<bound<<endl;
        COUT<<STR("NUM_PRC =")<<(num_files)<<endl;
        COUT<<STR("NUM_IGNORED_PRC =")<<(numNoTarget)<<endl;
        for(int k=0; k< num_files; k++)
            if(!isTarget[k])
            COUT<<STR("IGNORED_PRC =")<<converter.from_bytes(str_prc_name[k])<<endl;
        COUT<<STR("Z3_TIME_OUT =2000")<<endl;
        COUT<<STR("Z3_OPTIMIZATION_TIME_OUT =60000")<<endl;
        COUT<<STR("Z3_TIME_OUT_INCREASE =2000")<<endl;
        COUT<<STR("MAX_Z3_BISECTION_TIME_OUT =20000")<<endl;

  //%%%%%%  
        std::wofstream out("code.smt2");//translated file name
        std::wcout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!

	TimedSystem tsys;
        Idx ppta_idxs[num_files];
        int bvlens[num_files];

        for(int i=0; i< num_files; i++){
            ppta_idxs[i] = tsys.addAutomaton(&pptas[i]);
            String wfn = converter.from_bytes(str_file_name[i]);
            CCSProcess p(wfn, ppta_idxs[i]);
            tsys.addProcess(p);
            bvlens[i] = findBitvectorLength(pptas[i].getLocationsCount());
        }

        for(int iteration=1; iteration<= bound; iteration++){
            tsys.generateSmtFormulaSingleStep(iteration, bound);
        }
        
        // Reachability condition
        COUT<<STR("\n\n;;;===>>>Reachability Constraint\n\n");
	CHAR buf[BUFFER_SIZE];
        for(int i=0; i<num_files; i++){
            if(isTarget[i]){
                if(pptas[i].useBitVector)
                    SNPRINTF(buf, BUFFER_SIZE, STR("(assert (= %S_s_%d (_ bv%d %d)))\n"), pptas[i].getName().c_str(), 
                        bound, pptas[i].getLocationIndex(str_target_name[i]), bvlens[i]);
                else
                    SNPRINTF(buf, BUFFER_SIZE, STR("(assert (= %S_s_%d %d))\n"), pptas[i].getName().c_str(), 
                        bound, pptas[i].getLocationIndex(str_target_name[i]));
                    
                COUT<<buf;
            }
        }
        
        if(numNoTarget<num_files){
            COUT<<STR("\n\n;;;===>>>Optimization Constraint\n\n");
            String optStr = STR("(minimize (+ ");
            for(int i=0; i<num_files; i++){
                if(isTarget[i])
                    optStr += pptas[i].getName()+STR("_price_") + ::toString(bound) + STR(" ");
            }
            optStr += STR("))\n");
            COUT<<optStr;
        }
        COUT<<STR("\n\n;;;===>>>Satisfiability Check\n\n");
        COUT<<STR("(check-sat)\n");

        for(int i=0; i<num_files; i++){
            if(isTarget[i])
                COUT<< STR("(get-value (") + pptas[i].getName()+STR("_price_") + ::toString(bound) + STR("))") <<endl;
        }            
}

void generateCoraFile(PricedTimedAutomaton pta, String filename) {
	wofstream corafile;
	char f_name[BUFFER_SIZE];

	snprintf(f_name, BUFFER_SIZE, "%S", filename.c_str());

	corafile.open(f_name, ios::out);

	pta.toUppaalSystemString(corafile);
	corafile.close();
}

int pta_generator(int argc, char* argv[]) {
	PricedTimedAutomaton pta, power_pta;

	CHAR filename[BUFFER_SIZE];
	SNPRINTF(filename, BUFFER_SIZE, STR("%s"), argv[2]);
	String fname(filename);

	int power = atoi(argv[3]);

	pta.loadFromFile(fname);
        //COUT<< "using a mock PTA...\n";
        //COUT<< makeAutomata1().toString()<<"\n";
        //COUT<< makeAutomata1().generateSmtOptimization1Clause(2,3);

	power_pta = pta;
	power_pta.renameClocksWithSuffix(STR("1"));

	// Write in Uppaal CORA format
	generateCoraFile(power_pta, fname + String(STR(".1.cora.xml")));

	// Write in our format
	power_pta.writeToFile(fname + String(STR(".1.xml")));


	for(int i = 2; i <= power; i ++) {
		power_pta = power_pta * pta;
		power_pta.renameClocksWithSuffix(STR("1"));

		CHAR buf[BUFFER_SIZE];
		SNPRINTF(buf, BUFFER_SIZE, STR(".%d"), i);

		// Write in Uppaal CORA format
		generateCoraFile(power_pta, fname + buf + String(STR(".cora.xml")));

		// Write in our format
		power_pta.writeToFile(fname + buf + String(STR(".xml")));
	}

	return 0;
}

int ppta_generator(int argc, char* argv[]) {
	PolyPricedTimedAutomaton ppta, ppower_pta;

	CHAR filename[BUFFER_SIZE];
	SNPRINTF(filename, BUFFER_SIZE, STR("%s"), argv[2]);
	String fname(filename);

	ppta.loadFromFile(fname);

	ppower_pta = ppta;
	ppower_pta.renameClocksWithSuffix(STR("1"));

	// Write in our format
	ppower_pta.writeToFile(fname + String(STR(".1.xml")));

	return 0;
}

int print_pta(int argc, char* argv[]) {
	if(argc != 2) {
		CERR << STR("Usage: ") << argv[0] << STR(" filename")<<endl;
		return 1;
	}

	CHAR filename[BUFFER_SIZE];
	SNPRINTF(filename, BUFFER_SIZE, STR("%s"), argv[1]);

	PricedTimedAutomaton pta;
	pta.loadFromFile(String(filename));

	COUT << pta.toString();

	return 0;
}

void showCommandUsage(int argc, char* argv[]) {
	CERR << STR("Usage: \n");
	CERR << argv[0] << STR(" --pta-generator filename power")<<endl;
	CERR << argv[0] << STR(" --ta-generator filename power")<<endl;
	CERR << argv[0] << STR(" --pta-smt filename reach_loc price_expression bound")<<endl;
	CERR << argv[0] << STR(" --ta-smt filename reach_loc bound")<<endl;
	CERR << argv[0] << STR(" --ccs-smt max_step [-][process name 1] [-][goal state 1] ... [-][process name n] [-][goal state n] ")<<endl;
	CERR << argv[0] << STR(" --ccs-dreal max_step [-][process name 1] [-][goal state 1] ... [-][process name n] [-][goal state n] ")<<endl;
}

int main(int argc, char* argv[]) {
    
    //this is important for compatibility among different platforms 
    //to work with wide chars i.e. %s for char and %S for wchar_t
    std::setlocale(LC_ALL, "en_US.UTF-8");

        std::wstreambuf *coutbuf = std::wcout.rdbuf(); //save old buf

	if(argc < 2) {
		showCommandUsage(argc, argv);
		return 1;
	}

	CHAR buf[BUFFER_SIZE];

	//SNPRINTF(buf, BUFFER_SIZE, STR("%s"), argv[1]);
        std::mbstowcs(buf, argv[1], BUFFER_SIZE);
	String cmd_option = String(buf);

	if(cmd_option == String(STR("--pta-generator")) && argc == 4) {
		pta_generator(argc, argv);//--pta-generator pta.xml 1
	}
	else if(cmd_option == String(STR("--ppta-generator")) && argc == 3) {
		ppta_generator(argc, argv);//--ppta-generator ppta.xml
	}
	else if(cmd_option == String(STR("--pta-smt")) && argc == 5) {
			pta_smt(argc, argv);//--pta-smt pta.xml l2 5
		}
	else if(cmd_option == String(STR("--ta-smt")) && argc == 5) {
			ta_smt(argc, argv);//--ta-smt alp.xml s2 3
		}
	else if(cmd_option == String(STR("--ppta-smt")) && argc == 5) {
			ppta_smt(argc, argv);//--ppta-smt ppta-test-1.xml l2 5
		}
	else if(cmd_option == String(STR("--ccs-smt")) && argc >= 5) {
            //ccs_ta_smt(argc, argv);//--ccs-smt 20 ProductA End_job Bioreactor Idle Cleaner Idle Purifier Idle
            
            ccs_ppta_smt_iterative(argc, argv, false);//--ccs-smt 20 ProductA End_job Bioreactor Idle Cleaner Idle Purifier Idle
            
            //ccs_ta_smt_iterative(argc, argv);
            
            //--ccs-smt 20 ProductA ENDJOB Bioreactor Idle Cleaner Idle Purifier Idle
            //--ccs-smt 150 ProdA1 ENDJOB ProdA2 ENDJOB ProdA3 ENDJOB ProdA4 ENDJOB ProdA5 ENDJOB ProdA6 ENDJOB  ProdB1 ENDJOB ProdB2 ENDJOB ProdB3 ENDJOB ProdB4 ENDJOB ProdB5 ENDJOB ProdB6 ENDJOB Pur1 Idle Pur2 Idle Bio1 Idle Bio2 Idle Clean Idle 
            //--ccs-smt 2 ProdA1 ENDJOB Bio1 Idle Clean1 Idle Pur1 Idle
            //--ccs-smt 20 KL164 done KL157 done KL150 done KL143 done KL136 done KL129 done KL122 done KL115 done KL108 done KL101 done runway0 -loop runway1 -loop
            //--ccs-smt 20 Loop end
            std::wcout.rdbuf(coutbuf);
            cout << "\n======================================================================\n";
            cout << "\t\tTranslation is completed successfully!";
            cout << "\n======================================================================\n";

        }
	else if(cmd_option == String(STR("--ccs-dreal")) && argc >= 5) {
            
            ccs_ppta_smt_iterative(argc, argv, true);//--ccs-smt 20 ProductA End_job Bioreactor Idle Cleaner Idle Purifier Idle
            std::wcout.rdbuf(coutbuf);
            cout << "\n======================================================================\n";
            cout << "\t\tTranslation is completed successfully!";
            cout << "\n======================================================================\n";
        
        }
	else {
		showCommandUsage(argc, argv);
		return 1;
	}

	return 0;
}
